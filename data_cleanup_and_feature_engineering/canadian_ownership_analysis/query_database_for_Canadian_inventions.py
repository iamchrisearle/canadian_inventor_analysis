#####################################
##Querying the DB for all Inventors##
#####################################

"""
Code to query the DB to pull all Canadian inventions.
Will require access to a server holding the PATSTAT database.
"""

import pymysql
import paramiko
import pandas as pd
from paramiko import SSHClient
from sshtunnel import SSHTunnelForwarder
import time
####Increase display options for Pandas####
pd.options.display.max_columns = None
pd.options.display.max_rows = 150



def spinUp():
    ####Path in####
    path = "C:/Users/ccearle/Documents/cipo-paper.git/Code/"
    ####Port in Credentials####
    with open(path + "ssh_host.pem", 'r') as file:
        ssh_host = file.readline()
    with open(path + "ssh_user.pem", 'r') as file:
        ssh_username = file.readline()
    with open(path + "ssh_password.pem", 'r') as file:
        ssh_password = file.readline()
    with open(path + "mysql_username.pem", "r") as file:
        mysql_username = file.readline()
    with open(path + "mysql_password.pem", "r") as file:
        mysql_password = file.readline()
    with open(path + "mysql_db.pem", "r") as file:
        mysql_db = file.readline()
    ####Tunnel In####
    tunnel = SSHTunnelForwarder((ssh_host, 22), ssh_username= ssh_username, ssh_password = ssh_password, remote_bind_address=("localhost", 3306))
    tunnel.start()
    time.sleep(2)
    conn = pymysql.connect(host='127.0.0.1', user = mysql_username, passwd = mysql_password, db = mysql_db, port=tunnel.local_bind_port)
    return conn




#read in query file
with open("C:/Users/ccearle/Documents/cipo-paper.git/Code/Clean Code/Querying Related/CA_weighted_owned_innovations_assignee_imputed.txt", "r", encoding = 'utf-8') as file:
    ids = file.read().replace("[", "").replace("]","").replace("'", "").replace(" ", "").split(",")

def queryBasicData(query, file_name):
    #loop through query, add in ;
    counter = 0

    conn = spinUp()

    for id in ids:
        with open(file_name, "a+", encoding = "utf-8") as f:
            pd.read_sql_query(query.replace("\n", " ") + id + ";", conn).to_csv(f, header=f.tell()==0, encoding = "utf-8")
        #results = results.append(pd.read_sql_query(query.replace("\n", " ") + id + ";", conn))
        counter += 1
        print(f"Finished id: {id}. Finished {counter} ids of {len(ids)                               }", end = "\r")

    time.sleep(10)
    conn.close()



###########################
##Query for base_data set##
###########################

query = """SELECT tls201_appln.appln_id, appln_auth, appln_kind, appln_filing_date, appln_nr_original, earliest_filing_id,
    earliest_filing_date, docdb_family_id, granted, nb_citing_docdb_fam, docdb_family_size,
    nb_applicants, nb_inventors, tls206_person.person_id, person_name, person_address, person_ctry_code,
    psn_id, psn_name, psn_level, psn_sector, applt_seq_nr, invt_seq_nr
    FROM tls201_appln
    JOIN tls207_pers_appln ON tls201_appln.appln_id = tls207_pers_appln.appln_id
    JOIN tls206_person ON tls207_pers_appln.person_id = tls206_person.person_id
    WHERE earliest_filing_id =
    """

queryBasicData(query, "D:/PATSTAT Result Data Sets/Final Data Sets/Canadian_inventions_determined_by_second_loop.csv")


########################
##Query for NACE2 data##
########################

query_two = """SELECT  tls201_appln.appln_id, appln_auth, appln_kind, appln_filing_date, appln_nr_original, earliest_filing_id,
    earliest_filing_date, docdb_family_id, granted, nb_citing_docdb_fam, docdb_family_size, nb_applicants, nb_inventors,
    tls229_appln_nace2.nace2_code, weight
    FROM tls201_appln
    JOIN tls229_appln_nace2 ON tls201_appln.appln_id = tls229_appln_nace2.appln_id
    JOIN tls902_ipc_nace2 ON tls229_appln_nace2.nace2_code = tls902_ipc_nace2.nace2_code
    WHERE earliest_filing_id = """



#query out to NACE
queryBasicData(query_two, "D:/PATSTAT Result Data Sets/Final Data Sets/Canadian_inventions_determined_by_second_loop_NACE2.csv")


#########################
##Query for techn_field##
#########################


query_techn_field = """SELECT  tls201_appln.appln_id, techn_field_nr, weight
    FROM tls201_appln
    JOIN tls230_appln_techn_field ON tls201_appln.appln_id = tls230_appln_techn_field.appln_id
    WHERE earliest_filing_id = """


queryBasicData(query_techn_field, "D:/PATSTAT Result Data Sets/Final Data Sets/Canadian_inventions_from_assignee_imputation_techn_field_data_only.csv")



################################
##Query for NACE2 Lookup Table##
################################

with open("D:/PATSTAT Result Data Sets/Final Data Sets/NACE2_Table.csv", "a+", encoding = "utf-8") as f:
    pd.read_sql_query("SELECT * FROM tls902_ipc_nace2;", spinUp()).to_csv(f, header=f.tell()==0, encoding = "utf-8")



######################################
##Query for Techn_field lookup table##
######################################

with open("D:/PATSTAT Result Data Sets/Final Data Sets/Techn_Field_Table.csv", "a+", encoding = "utf-8") as f:
    pd.read_sql_query("SELECT * FROM tls901_techn_field_ipc;", spinUp()).to_csv(f, header=f.tell()==0, encoding = "utf-8")




##############################
##Query for publication data##
##############################

query_publication_data ="""SELECT tls201_appln.appln_id, earliest_filing_id, publn_first_grant, publn_date
    FROM tls201_appln
    JOIN tls211_pat_publn ON tls201_appln.appln_id = tls211_pat_publn.appln_id
    WHERE publn_first_grant = 'Y' AND earliest_filing_id = """



    queryBasicData(query_publication_data, "D:/PATSTAT Result Data Sets/Final Data Sets/publication_table_data.csv")




#########################################
##Look at Innovations with No NACE data##
#########################################

NACE_data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Canadian_inventions_determined_by_second_loop_NACE2.csv", index_col = 0)


int_ids = [int(i) for i in ids]
not_found = list(set(int_ids) - set(NACE_data["earliest_filing_id"]))


pd.read_sql_query(query_two + str(ids[3]) + ";", spinUp())
