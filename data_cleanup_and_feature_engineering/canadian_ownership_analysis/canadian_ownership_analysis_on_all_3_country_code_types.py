"""
Determines the list of earliest filing ids to query from the db in a few different ways:
1) Baseline method, pulling an efid for any innovation where a Canadian inventor is mentioned
2) Baseline weighted method, pulling efid for an innovation with >50% CA country codes listed
3) Imputed EFID method, pulling efid from an innovation with >50% CA imputed country code listed
4) Imputed Assignee method, pulling efid from an innovation with >50% CA assignee imputed country code listed
5) Imputed Assignee Second Loop EFID Method from an innovation with >50% CA assignee imputed country code listed
"""

import pandas as pd

dtypeDict = {"appln_id":"int64", "earliest_filing_id":"int64", "psn_id":"int64", "applt_seq_nr":"int64", "invt_seq_nr":"int64", "assignee_id":"int64", "Unnamed: 0":"int64"}
data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/master_data_set_merged_imputed_efid_assignee_second_loop.csv", low_memory=False)
#rename this columns - this should have been done a lot earlier as it's confusing.
data.rename(columns = {"imputed_country_efid_y" : "imputed_within_assignee"}, inplace = True)
data.dtypes

#########################################
##Baseline - Just one Canadian Inventor##
#########################################

def findInnovationOwnerBaseline(data):
    #just returns the innovations where any Canadian was lsited
    return set(data[(data["person_ctry_code"] == "CA") & (data["invt_seq_nr"] > 0)]["earliest_filing_id"])

CA_inventor_contributed_innovations = findInnovationOwnerBaseline(data)

#export this list for querying
with open("C:/Users/ccearle/Documents/cipo-paper.git/Code/Clean Code/Querying Related/CA_inventor_contributed_innovations.txt", "a+", encoding = "utf-8") as file:
    file.write(str([str(i) for i in CA_inventor_contributed_innovations]))


#find the country of owner for each INNOVATION level
def findInnovationOwnerWeightedImputed(data, inventor_country, new_column_name):
    """
    * 'data' is just a data set
    * 'inventor_country' is the column name you would like to use as the country code. e.g. the patstat native code, imputed from efid, imputed from assignee etc.
        current countries options include:
        - person_ctry_code
        - imputed_country_efid
        - imputed_within_assignee
        - imputed_country_efid_second_loop
    * 'new_column_name' is a string that the new column will be named (usually just the inventor country column + 'weight')
    """
    data = data[data["invt_seq_nr"] > 0]
    max_country_efid_baseline = data.groupby("earliest_filing_id")[inventor_country].value_counts(normalize = True)
    max_country_efid_baseline_df = pd.DataFrame(max_country_efid_baseline)
    max_country_efid_baseline_df.rename(columns = {inventor_country:new_column_name}, inplace = True)
    max_country_efid_baseline_df.reset_index(inplace = True)
    CA_majority_invented_inventions = max_country_efid_baseline_df[(max_country_efid_baseline_df[new_column_name] > 0.5) & (max_country_efid_baseline_df[inventor_country] == "CA")]
    CA_owned_innovations_baseline = set(CA_majority_invented_inventions["earliest_filing_id"])
    return CA_owned_innovations_baseline


###############################
##From the weighted base-case##
###############################
CA_person_ctry_code = findInnovationOwnerWeightedImputed(data, "person_ctry_code", "person_ctry_code_weighted")

#Export
with open("C:/Users/ccearle/Documents/cipo-paper.git/Code/Clean Code/Querying Related/CA_weighted_owned_innovations_baseline.txt", "a+", encoding = "utf-8") as file:
    file.write(str([str(i) for i in CA_person_ctry_code]))


##############################
###from the first EFID Loop###
##############################
CA_imputed_country_efid = findInnovationOwnerWeightedImputed(data, "imputed_country_efid", "imputed_country_efid_weighted")

#Export
with open("C:/Users/ccearle/Documents/cipo-paper.git/Code/Clean Code/Querying Related/CA_weighted_owned_innovations_imputed.txt", "a+", encoding = "utf-8") as file:
    file.write(str([str(i) for i in CA_imputed_country_efid]))


##########################################
##From the assignee_imputation step only##
##########################################

CA_imputed_within_assignee = findInnovationOwnerWeightedImputed(data, "imputed_within_assignee", "imputed_within_assignee_weighted")

with open("C:/Users/ccearle/Documents/cipo-paper.git/Code/Clean Code/Querying Related/CA_weighted_owned_innovations_assignee_imputed.txt", "a+", encoding = "utf-8") as file:
    file.write(str([str(i) for i in CA_imputed_within_assignee]))


#########################################
##CA_weighted_owned_innovations_imputed##
#########################################

CA_weighted_owned_innovations_assignee_imputed_second_loop = findInnovationOwnerWeightedImputed(data, "imputed_country_efid_second_loop", "imputed_country_efid_second_loop_weighted")

with open("C:/Users/ccearle/Documents/cipo-paper.git/Code/Clean Code/Querying Related/CA_weighted_owned_innovations_assignee_imputed_second_loop.txt", "a+", encoding = "utf-8") as file:
    file.write(str([str(i) for i in CA_weighted_owned_innovations_assignee_imputed_second_loop]))




#Check lengths of the outputs to see the differences between the methods#
print(len(CA_inventor_contributed_innovations))
print(len(CA_person_ctry_code))
print(len(CA_imputed_country_efid))
print(len(CA_imputed_within_assignee))
print(len(CA_weighted_owned_innovations_assignee_imputed_second_loop))
