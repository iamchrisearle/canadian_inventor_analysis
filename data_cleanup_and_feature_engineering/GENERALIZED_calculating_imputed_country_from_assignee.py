##############################
##Assignee Matcher on Subset##
##############################

"""
Takes in a null and not-null SUBSET
SUBSET is created from the subsetting_data_by_high_employee_count_assignees.py file
Pairs up every unknown inventor name to an inventor name and calculates the fuzzy distance between names
Does the assignee match on these inventions.

!!
This computation will be very large (quadrillions of rows of data)
This code is a general format and will work for smaller data sets
It will require massive computing power if run on the entire 2001-2016 dataset
!!
"""




import pandas as pd
import dask.dataframe as dd
from fuzzywuzzy import fuzz
import time
import os
from dask.distributed import Client
client = Client()
client


not_null_cols = ["earliest_filing_id","appln_filing_date", "appln_id", "psn_name", "person_ctry_code", "assignee_id", "imputed_country_efid"]
nul_cols = ["earliest_filing_id", "appln_filing_date", "appln_id", "psn_name", "person_ctry_code", "appln_psn_id", "assignee_id", "imputed_country_efid"]

dtypeDict = {"appln_id":"int64", "earliest_filing_id":"int64", "psn_id":"int64", "applt_seq_nr":"int64", "invt_seq_nr":"int64", "assignee_id":"int64", "appln_psn_id":"int64", "person_ctry_code":"object", "imputed_country_efid":"object"}

null_country = pd.read_csv("any_null_country_inventor_only.csv", usecols = nul_cols,low_memory=False, dtype=dtypeDict)
not_null_country = pd.read_csv("not_null_country.csv", low_memory=False, usecols = not_null_cols, dtype=dtypeDict)


not_null_country.drop_duplicates(["psn_name", "appln_filing_date", "imputed_country_efid"], keep="first", inplace = True)

checkList = list(null_country["assignee_id"].unique())


def fuzzMatcher(x, y):
    return fuzz.token_set_ratio(x, y)

def highlyMatchedFinder(not_null_country, null_country):
    counter =  0
    total_time = 0
    null_country.set_index("assignee_id", inplace=True)
    null_country.sort_index(inplace = True)
    null_country = dd.from_pandas(null_country, npartitions = ncpus)
    ##
    not_null_country.set_index("assignee_id", inplace = True)
    not_null_country.sort_index(inplace = True)
    not_null_country = dd.from_pandas(not_null_country, npartitions = ncpus)
    ##
    for item in checkList:
        print(f"working on item {item}")
        start = time.time()
        ##
        print("subsetting")
        null_country_df = null_country.loc[item]
        not_null_country_df = not_null_country.loc[item]
        mergeSize = len(null_country_df)*len(not_null_country_df)
        #pair up the names on the same invention
        print(f"merging on {mergeSize} rows")
        workingMatchDF = null_country_df.join(not_null_country_df, on = "assignee_id", how = "inner", lsuffix='_x', rsuffix='_y').compute(scheduler = "processes")
        workingMatchDF = dd.from_pandas(workingMatchDF.reset_index(), npartitions = ncpus)
        del null_country_df
        del not_null_country_df
        #fuzz match all the pairwise names
        print("fuzzy matching")
        workingMatchDF["fuzz"] = workingMatchDF.map_partitions(lambda df: df.apply(lambda row: fuzzMatcher(row.psn_name_x, row.psn_name_y), axis = 1)).compute(scheduler = "processes")
        print("computing")
        highlyMatchedDF = workingMatchDF[workingMatchDF["fuzz"] > 90].compute()
        print("exporting")
        with open("highly_matched_df_from_assignee_subset_" + str(item) + ".csv", "a", encoding = "utf-8") as f:
            highlyMatchedDF.to_csv(f, header=f.tell()==0, encoding = "utf-8")
        duration = time.time() - start
        total_time += duration
        counter += 1
        print(f"Chunk of {counter} of 5,000 finished matching {len(workingMatchDF)} rows in {duration} seconds")
        del workingMatchDF
        del highlyMatchedDF
            counter += 1
    print(f"Process took {total_time} seconds")
    #return highlyMatchedDF.compute()

highlyMatchedDF = highlyMatchedFinder(not_null_country, null_country)
