###################################
####Calculates Imputed Country ####
###################################


"""
Takes in the dataframes from the various Data Quadrants as subsets out from
the preprocessing_for_same_invention_country_country_assigner.py file
Uses fuzzy matching on inventors within an invention to imputed inventor countries
from weighted counts of inventors.
"""


import pandas as pd
from fuzzywuzzy import fuzz
import time
pd.set_option("mode.chained_assignment", None)


columns = ['appln_id', 'appln_filing_date', 'appln_auth', 'earliest_filing_id', 'person_address','person_ctry_code', 'psn_id', 'psn_name', 'applt_seq_nr', 'invt_seq_nr','assignee', 'appln_psn_id']

null_country = pd.read_csv("nullCountryCodesFromMixedEFIDNullNotNullSet.csv", low_memory=False, usecols = columns, dtype={"appln_psn_id":"int64"})
not_null_country = pd.read_csv("NOTnullCountryCodesFromMixedEFIDNullNotNullSet.csv", low_memory=False, usecols = columns, dtype={"appln_psn_id":"int64"})

def highlyMatchedFinder(null_country, not_null_country):
    #create an empty df
    highlyMatched = pd.DataFrame()
    #declare which cols to pull
    not_null_cols = ["earliest_filing_id", "appln_id", "psn_name", "person_ctry_code", "appln_filing_date"]
    nul_cols = ["earliest_filing_id", "appln_id", "psn_name", "person_ctry_code", "appln_psn_id", "appln_filing_date"]
    start = time.time()
    #pair up the names on the same invention
    workingMatchDF = pd.merge(left = chunk[not_null_cols], right = null_country[nul_cols], on = "earliest_filing_id")
    #fuzz match all the pairwise names
    workingMatchDF["fuzz"] = workingMatchDF[["psn_name_x", "psn_name_y"]].apply(lambda x: fuzz.token_set_ratio(x["psn_name_x"], x["psn_name_y"]), axis = 1)
    #only retain names above the 90 fuzz ratio level
    highlyMatched = highlyMatched.append(workingMatchDF[workingMatchDF["fuzz"] > 90].set_index(["appln_psn_id", "appln_id_x"]).sort_index(level=["appln_psn_id", "appln_id_x"]))
    duration = time.time() - start
    print(f"Process took {total_time} seconds")
    return highlyMatched

#Took about 4.16 hours to run on my machine (i7, 8th gen, 64GB RAM)
highlyMatched = highlyMatchedFinder(null_country, not_null_country)

highlyMatched.reset_index().to_csv("highlyMatchedDF.csv")
