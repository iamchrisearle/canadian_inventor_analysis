"""
Bring in the full data set and the imputed EFID results
Merge the EFID results onto the full dataset and export the new data set
"""


import pandas as pd
import numpy as np
pd.options.display.max_columns = None
pd.options.display.max_rows = 150

##############################################
##Use this section for imputing EFID country##
##############################################


#bring in the full data set with assignees and indexers
dtypeDict = {"appln_id":"int64", "earliest_filing_id":"int64", "psn_id":"int64", "applt_seq_nr":"int64", "invt_seq_nr":"int64", "assignee_id":"int64", "appln_psn_id":"int64"}
data = pd.read_csv("allInventors2001-2016WithPSN_with_Indexer_and_Assignee_ID.csv", dtype = dtypeDict, low_memory=False)
#data.drop(columns = "Unnamed: 0", inplace = True)

#bring in the imputed country code data
imputed_country_efid = pd.read_csv("imputed_countries_from_efid_method.csv", dtype = dtypeDict, low_memory=False, usecols=["appln_psn_id", "person_ctry_code_x"])
imputed_country_efid = imputed_country_efid.rename(columns = {"person_ctry_code_x" : "imputed_country_efid"})

#set indicies to do the join
data.set_index("appln_psn_id", inplace = True)
imputed_country_efid.set_index("appln_psn_id", inplace = True)

#join the imputed efids onto the master dataset
data_with_imputed_efid = data.join(imputed_country_efid, on = "appln_psn_id", how = "outer")

#fill the NAs with the data given from PATSTAT
data_with_imputed_efid["imputed_country_efid"] = data_with_imputed_efid["imputed_country_efid"].fillna(data_with_imputed_efid["person_ctry_code"])

#Export the merged file to be used in creating the assignee.
data_with_imputed_efid.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/data_master_with_merged_imputed_efid.csv")
