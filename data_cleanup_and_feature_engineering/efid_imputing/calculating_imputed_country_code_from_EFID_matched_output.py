####################################
##Calculating Imputed Country Code##
####################################

"""Takes in the highly matched DF.
Convert application dates to datetime objects.
Parses out 9999 dates by finding rows with non-null data only.
Finds the days between.
Performs the grouby and finds the min dates.
THIS PROCESS WORKS ONLY ON EFIDFS
"""


import pandas as pd
import dask.dataframe as dd

data = pd.read_csv("highlyMatchedDF.csv", dtype = {"person_ctry_code_x":"category", "person_ctry_code_x":"category"}, low_memory=False)


def imputeCountryCodeAssignee(highlyMatched):
    #reset index and sort values first on the person id, then on the fuzz ratio
    data = highlyMatched.reset_index().sort_values(by = ["appln_psn_id", "fuzz"], ascending=False).copy()
    #after sorting, retain only the highest matched name for each person
    data.drop_duplicates(["appln_psn_id", "appln_id_x"], inplace = True)
    #convert to datetime objects
    data["appln_filing_date_x"] = pd.to_datetime(data["appln_filing_date_x"], errors="coerce")
    data["appln_filing_date_y"] = pd.to_datetime(data["appln_filing_date_y"], errors = "coerce")
    #create days between column
    data = data[(data["appln_filing_date_x"].notnull()) & (data["appln_filing_date_y"].notnull())]
    #convert into a dask for multiprocessing
    data = dd.from_pandas(data, npartitions = 12)
    #calculated the days between
    data["days_between"] = data[["appln_filing_date_x", "appln_filing_date_y"]].apply(lambda x: abs(x["appln_filing_date_x"] - x["appln_filing_date_y"]), axis = 1, meta = ("days_between", "timedelta64[ns]"))
    #retain only the lowest number of days between per person
    imputedCountries = data.groupby(["appln_psn_id", "person_ctry_code_x"])["days_between"].min()
    #run the computation
    imputedCountries = imputedCountries.compute()
    #reset the index
    imputedCountriesDF = pd.DataFrame(imputedCountries).reset_index()
    return imputedCountriesDF

imputedCountriesDF = imputeCountryCodeAssignee(testing)



def cleanupDuplicates(imputedCountriesDF):
    #Create a list of all the instances where there is no duplicates
    clean_set = imputedCountriesDF.drop_duplicates("appln_psn_id", keep = False)
    #find the instances where the person and the days between applications is the same on a duplicated row
    #These have no way of being determined unless we do weighted.
    drop_index = imputedCountriesDF[imputedCountriesDF.duplicated(subset =["appln_psn_id", "days_between"], keep = False)].sort_values("appln_psn_id").index
    #Drop these from the dataset altogether
    #They will be retained in the second iteration
    imputed_country_keep = imputedCountriesDF.loc[~imputedCountriesDF.index.isin(drop_index)]
    #But if there are duplicates people/countries but unique days between, I can pull the min one.
    appendDF = imputed_country_keep[imputed_country_keep.duplicated(subset ="appln_psn_id", keep = False)].groupby("appln_psn_id")["person_ctry_code_x", "days_between"].min().reset_index()
    imputed_country_clean = clean_set.append(appendDF)
    return imputed_country_clean

cleaned_countries = cleanupDuplicates(imputedCountriesDF)

cleaned_countries.to_csv("imputed_countries_from_efid_method.csv")
