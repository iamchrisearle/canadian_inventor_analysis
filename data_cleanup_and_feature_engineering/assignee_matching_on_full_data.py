####################
##Assignee Creator##
####################


"""
Bring in the Full Data set
Uses the Assignee Creator function to determine the assignee.
Will export the DataFiles afterwards as new DataSets to the .
"""

import pandas as pd

cols = ["appln_id", "appln_filing_date", "earliest_filing_id", "appln_auth", "person_address", "person_ctry_code", "psn_name", "applt_seq_nr", "invt_seq_nr", "psn_id"]

data = pd.read_csv("allInventors2001-2016WithPSN.csv", usecols = cols)


def assigneeAssigner(df):
    """Takes in a dataframe and attempts to find where the applt_seq_nr == 1
    Makes a sub df from this and breaks it down to just an appln_it and a psn_name
    This is then masked on the original dataframe.
    Checks for NaNs in the new column and subsets the NaNs to check for invt_seq_nr == 1
    Creates a second mask from this which is added to the first mask.
    The new mask is applied to the original df to generate a final assignee column
    The column shows who the owner of the patent is line by line"""
    #grab a df of all the known patent owners
    print("Locating known application owners")
    assigneeDF = df[df["applt_seq_nr"] == 1][["appln_id", "psn_name"]]
    assigneeDF.rename(columns = {"psn_name":"assignee"}, inplace = True)

    #merge the matches onto the original df
    print("Merging owners")
    PatentsWithAssignee = pd.merge(left = df, right = assigneeDF, on='appln_id', how='left')
    ghostMatch = pd.merge(left = df, right = assigneeDF, on='appln_id', how='left')

    #Handling edge cases
    #Which ones from the merge didn't match?
    print("Confirming matches")
    noOwnerDF = ghostMatch[ghostMatch["assignee"].isna()]

    #for the ones that didn't match, just set the owner as the primary inventor
    print("Finding first inventors where no owner was located")
    assigneeDF2 = noOwnerDF[noOwnerDF["invt_seq_nr"] == 1][["appln_id", "psn_name"]]
    #just change the psn to the assignee
    assigneeDF2.rename(columns = {"psn_name":"assignee"}, inplace = True)

    print("Building the new masking")
    #Now I have 2 maskings, just piece them together
    assigneeDF3 = assigneeDF.append(assigneeDF2)

    print("Merging new mask")
    #merge the new mask on. It will create some duplicate columns.
    PatentsWithAssignee = pd.merge(left = df, right = assigneeDF3, on='appln_id', how='left')

    return PatentsWithAssignee


def assigneeCodeAssigner(df):
    """Takes in a dataframe and attempts to find where the applt_seq_nr == 1
    Makes a sub df from this and breaks it down to just an appln_it and a psn_name
    This is then masked on the original dataframe.
    Checks for NaNs in the new column and subsets the NaNs to check for invt_seq_nr == 1
    Creates a second mask from this which is added to the first mask.
    The new mask is applied to the original df to generate a final assignee column
    The column shows who the owner of the patent is line by line"""
    #grab a df of all the known patent owners
    print("Locating known application owners")
    assigneeDF = df[df["applt_seq_nr"] == 1][["appln_id", "psn_id"]]
    assigneeDF.rename(columns = {"psn_id":"assignee_id"}, inplace = True)

    #merge the matches onto the original df
    print("Merging owners")
    PatentsWithAssignee = pd.merge(left = df, right = assigneeDF, on='appln_id', how='left')
    ghostMatch = pd.merge(left = df, right = assigneeDF, on='appln_id', how='left')

    #Handling edge cases
    #Which ones from the merge didn't match?
    print("Confirming matches")
    noOwnerDF = ghostMatch[ghostMatch["assignee_id"].isna()]

    #for the ones that didn't match, just set the owner as the primary inventor
    print("Finding first inventors where no owner was located")
    assigneeDF2 = noOwnerDF[noOwnerDF["invt_seq_nr"] == 1][["appln_id", "psn_id"]]
    #just change the psn to the assignee
    assigneeDF2.rename(columns = {"psn_id":"assignee_id"}, inplace = True)

    print("Building the new masking")
    #Now I have 2 maskings, just piece them together
    assigneeDF3 = assigneeDF.append(assigneeDF2)

    print("Merging new mask")
    #merge the new mask on. It will create some duplicate columns.
    PatentsWithAssignee = pd.merge(left = df, right = assigneeDF3, on='appln_id', how='left')

    return PatentsWithAssignee

dataAssignees = assigneeAssigner(data)

dataAssignees = assigneeCodeAssigner(data)


dataAssignees.to_csv("allInventors2001-2016WithPSN_with_Assignee_IDs.csv")
