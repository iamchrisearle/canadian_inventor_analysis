import os
import numpy as np
import pymysql
import paramiko
import pandas as pd
from paramiko import SSHClient
from sshtunnel import SSHTunnelForwarder
import time
####Increase display options for Pandas####
pd.options.display.max_columns = None
pd.options.display.max_rows = 150

####################################################
##Existing available data from the master data set##
####################################################

main_data_use_cols = ["appln_id", "appln_auth", "appln_kind", "appln_filing_date", "appln_nr_original", "earliest_filing_id",
    "earliest_filing_date", "docdb_family_id", "granted", "nb_citing_docdb_fam", "docdb_family_size",
    "nb_applicants", "nb_inventors"]

main_data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/allInventors2001-2016WithPSN.csv", usecols = main_data_use_cols)
main_data.drop_duplicates(inplace = True)

main_data.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Full Data Set Regressions/main_data.csv")


#################################
##Inventor and Applicant Tables##
#################################

"""
The inventor data was reduced during the looping processes.
To obtain the right columns, a merge will be done on
the original full data set with the second loop set
"""

#set import columns from second loop data set
inventor_data_use_cols = ["appln_id", "psn_id", "psn_name", "person_address",
    "person_ctry_code", "imputed_country_efid_second_loop", "applt_seq_nr", "invt_seq_nr", "appln_psn_id"]

#import second loop data set
inventor_data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/master_data_set_merged_imputed_efid_assignee_second_loop.csv", usecols = inventor_data_use_cols)

#set columns for earlier query set
inventor_data_two_use_cols = ["appln_id", "psn_id", 'psn_sector', 'person_id', 'psn_level', 'person_name']

#import
inventor_data_two = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/allInventors2001-2016WithPSN.csv", usecols = inventor_data_two_use_cols)

#create appln_psn_id
inventor_data_two["appln_psn_id"] = inventor_data_two[["appln_id", "psn_id"]].apply(lambda x: int(str(x.appln_id) + str(x.psn_id)), axis = 1)

#merge the needed variables on the main set
inventor_data_meged = pd.merge(left = inventor_data, right = inventor_data_two[["appln_psn_id", 'psn_sector', 'person_id', 'psn_level', 'person_name']], on = "appln_psn_id", how = "left")

inventor_data_meged.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Full Data Set Regressions/person_data.csv")


##########################################
##Import Base Technology and NACE tables##
##########################################

#import NACE2 table for matching#
NACE_names = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/NACE2_Table.csv", usecols = ["nace2_code", "nace2_descr"])
NACE_names.drop_duplicates("nace2_code", inplace = True)
NACE_names.dropna(inplace = True)

#Import intensity metric#
intensity_data = pd.read_csv("C:/Users/ccearle/Documents/cipo-paper.git/Code/Clean Code/Research Intensity Measures/research_intensity_metric_from_output.csv", dtype = {"industry":"int64"})
intensity_data.rename(columns = {"value_added":"output"}, inplace = True)

#import techn_field lookup table
techn_field_lookup_table = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Techn_Field_Table.csv", usecols = ["techn_field_nr", "techn_field"])
techn_field_lookup_table.drop_duplicates(inplace = True)


###############
##Query Setup##
###############


def spinUp():
    ####Path in####
    path = "C:/Users/ccearle/Documents/cipo-paper.git/Code/"
    ####Port in Credentials####
    with open(path + "ssh_host.pem", 'r') as file:
        ssh_host = file.readline()
    with open(path + "ssh_user.pem", 'r') as file:
        ssh_username = file.readline()
    with open(path + "ssh_password.pem", 'r') as file:
        ssh_password = file.readline()
    with open(path + "mysql_username.pem", "r") as file:
        mysql_username = file.readline()
    with open(path + "mysql_password.pem", "r") as file:
        mysql_password = file.readline()
    with open(path + "mysql_db.pem", "r") as file:
        mysql_db = file.readline()
    ####Tunnel In####
    tunnel = SSHTunnelForwarder((ssh_host, 22), ssh_username= ssh_username, ssh_password = ssh_password, remote_bind_address=("localhost", 3306))
    tunnel.start()
    time.sleep(2)
    conn = pymysql.connect(host='127.0.0.1', user = mysql_username, passwd = mysql_password, db = mysql_db, port=tunnel.local_bind_port)
    return conn



def queryBasicData(query, data, file_name):
    #loop through query, add in ;
    counter = 0

    conn = spinUp()

    applications = data["appln_id"].unique()

    for application in applications:
        with open(file_name, "a+", encoding = "utf-8") as f:
            pd.read_sql_query(query.replace("\n", " ") + str(application) + ";", conn).to_csv(f, header=f.tell()==0, encoding = "utf-8")
        #results = results.append(pd.read_sql_query(query.replace("\n", " ") + id + ";", conn))
        counter += 1
        print(f"Finished application: {application}. Finished {counter} ids of {len(applications)                               }", end = "\r")

    time.sleep(10)
    conn.close()




def largeQueryBasicData(query, file_name):
    conn = spinUp()

    with open(file_name, "a+", encoding = "utf-8") as f:
        pd.read_sql_query(query.replace("\n", " ") + ";", conn).to_csv(f, header=f.tell()==0, encoding = "utf-8")

    time.sleep(10)
    conn.close()


###########
##Queries##
###########


nace_query = """
    SELECT tls201_appln.appln_id, earliest_filing_id, tls229_appln_nace2.nace2_code, weight
    FROM tls201_appln
    JOIN tls229_appln_nace2 ON tls201_appln.appln_id = tls229_appln_nace2.appln_id
    WHERE earliest_filing_date >= "2001-03-01" AND earliest_filing_date < "2017-01-01" """


techn_query = """SELECT  tls201_appln.appln_id, techn_field_nr, weight
    FROM tls201_appln
    JOIN tls230_appln_techn_field ON tls201_appln.appln_id = tls230_appln_techn_field.appln_id
    WHERE earliest_filing_date >= "2001-03-01" AND earliest_filing_date < "2017-01-01" """



largeQueryBasicData(nace_query, "D:/PATSTAT Result Data Sets/Final Data Sets/Full Data Set Regressions/nace_data.csv")

largeQueryBasicData(techn_query, "D:/PATSTAT Result Data Sets/Final Data Sets/Full Data Set Regressions/techn_data.csv")



query_publication_data ="""SELECT tls201_appln.appln_id, appln_nr, appln_nr_original, docdb_family_id,
    publn_first_grant, publn_nr, publn_nr_original, publn_date
    FROM tls201_appln
    JOIN tls211_pat_publn ON tls201_appln.appln_id = tls211_pat_publn.appln_id
    WHERE earliest_filing_date >= "2001-03-01" AND earliest_filing_date < "2017-01-01" """

largeQueryBasicData(query_publication_data, "D:/PATSTAT Result Data Sets/Final Data Sets/Full Data Set Regressions/publn_data.csv")




query_continuation = """SELECT * FROM tls201_appln
    JOIN tls216_appln_contn ON tls201_appln.appln_id = tls216_appln_contn.appln_id
    WHERE earliest_filing_date >= "2001-03-01" AND earliest_filing_date < "2017-01-01" """


largeQueryBasicData(query_continuation, "D:/PATSTAT Result Data Sets/Final Data Sets/Full Data Set Regressions/query_continuation.csv")

############################
##Cleanup and Merging Data##
############################


nace_data =  pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Full Data Set Regressions/nace_data.csv", index_col =0)

def industry2DigitCreator(df):
    #Shorten ISIC codes to create a nace industry code
    df["industry"] = df.apply(lambda x: int(str(x["nace2_code"]).split(".")[0]), axis = 1)
    return df

nace_data = industry2DigitCreator(nace_data)

def intensityMeasureCreation(df):

    #make one intensity metric per application
    df["weighted_intensity"] =  df["weight"]*df["intensity"]

    #merged the sum of the weighted intensity measure per applicaiton onto the data
    NACE_appln_int = pd.merge(left = df, right = df.groupby("appln_id")["weighted_intensity"].sum(skipna = False).reset_index().rename(columns = {"weighted_intensity":"application_intensity"}), on = "appln_id", how = "left")

    #average the application intensity over the innovation to get the innovation intensity
    NACE_efid_int = pd.merge(left = NACE_appln_int, right = NACE_appln_int.groupby("earliest_filing_id")["application_intensity"].mean().reset_index().rename(columns = {"application_intensity":"innovation_intensity"}), on = "earliest_filing_id", how = "left")

    """
    For instances with intensity measure of NaN, the NaN is being weighted into the mean() function and skipna = False
    does not work without tossing an error. To solve, find intensity = NaN and rewrite applicaiton intensity
    and innovation intensity as NaNs.
    """

    #Find the applications and innovations with NaN intensity
    applications_nan_intensity = NACE_efid_int[NACE_efid_int["intensity"].isna()]["appln_id"].unique()
    innovations_nan_intensity = NACE_efid_int[NACE_efid_int["intensity"].isna()]["earliest_filing_id"].unique()

    #Rewrite those application and innovation intensities as NaNs
    NACE_efid_int.at[NACE_efid_int[NACE_efid_int["appln_id"].isin(applications_nan_intensity)].index, "application_intensity"] = np.nan
    NACE_efid_int.at[NACE_efid_int[NACE_efid_int["earliest_filing_id"].isin(innovations_nan_intensity)].index, "innovation_intensity"] = np.nan

    return NACE_efid_int

nace_with_intensity_data = pd.merge(left = nace_data, right = intensity_data, on = "industry", how = "left" )

research_intensity_data = intensityMeasureCreation(nace_with_intensity_data)

research_intensity_data = research_intensity_data[["appln_id", "application_intensity", "innovation_intensity"]]

research_intensity_data.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Full Data Set Regressions/intensity_data.csv")

######################
##Dummyize NACE Data##
######################

def dummyizeNACEFields(df):
    #merged the names onto the
    NACE_full_desc = pd.merge(left = df, right = NACE_names, on = "nace2_code", how = "left")

    dummyize = pd.DataFrame(NACE_full_desc.groupby(["appln_id", "nace2_descr"])["weight"].sum()).reset_index().pivot(index = "appln_id" , columns = "nace2_descr")
    dummyize.columns = dummyize.columns.droplevel(0)
    dummyize = dummyize.reset_index()
    dummyize = dummyize.fillna(int(0))

    return dummyize

nace_data_dummyized_one = dummyizeNACEFields(nace_data[nace_data["appln_id"] < 290247687][["appln_id", "nace2_code", "weight"]])
nace_data_dummyized_two = dummyizeNACEFields(nace_data[nace_data["appln_id"] >= 290247687][["appln_id", "nace2_code", "weight"]])

nace_data_dummyized = nace_data_dummyized_one.append(nace_data_dummyized_two)

nace_data_dummyized.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Full Data Set Regressions/nace_data_percentage_dummies.csv")

#############################
##Dummyize Technology Field##
#############################

def dummyizeTechnFields(df):
    techn_full_desc = pd.merge(left = df, right = techn_field_lookup_table, on = "techn_field_nr", how = "left")

    dummyize_tech = pd.DataFrame(techn_full_desc.groupby(["appln_id", "techn_field"])["weight"].sum()).reset_index().pivot(index = "appln_id" , columns = "techn_field")
    dummyize_tech.columns = dummyize_tech.columns.droplevel(0)
    dummyize_tech = dummyize_tech.reset_index()
    dummyize_tech = dummyize_tech.fillna(int(0))

    return dummyize_tech


techn_data =  pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Full Data Set Regressions/techn_data.csv", index_col =0)

techn_field_lookup_table

techn_data_dummyized = dummyizeTechnFields(techn_data)

techn_data_dummyized.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Full Data Set Regressions/techn_data_percentage_dummies.csv")
