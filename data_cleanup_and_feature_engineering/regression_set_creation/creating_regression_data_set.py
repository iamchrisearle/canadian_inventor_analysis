"""
# inventors
Applicant ownership type
# citations
# offices
Year

Industry
Tech class
Intensity
CA first applicant?


Add in both NACE2 and Technology field text (36 group) to data sets
"""

import pandas as pd
import numpy as np
pd.options.display.max_columns = None
pd.options.display.max_rows = 150

#import base data, reduce down to regular applications only#
base_data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Canadian_inventions_determined_by_second_loop_clean.csv", index_col = 0)
base_data = base_data[base_data["appln_kind"] == "A"]

#import the NACE data from the earliest filing IDs#
NACE_data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Canadian_inventions_determined_by_second_loop_NACE2.csv", usecols = ["appln_kind", "appln_id", "earliest_filing_id", "nace2_code", "weight"])
NACE_data.drop_duplicates(inplace = True)
NACE_data = NACE_data[NACE_data["appln_kind"] == "A"].drop(columns = "appln_kind")

#import NACE2 table for matching#
NACE_names = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/NACE2_Table.csv", usecols = ["nace2_code", "nace2_descr"])
NACE_names.drop_duplicates("nace2_code", inplace = True)
NACE_names.dropna(inplace = True)

#Import intensity metric#
intensity_data = pd.read_csv("C:/Users/ccearle/Documents/cipo-paper.git/Code/Clean Code/Research Intensity Measures/research_intensity_metric_from_output.csv", dtype = {"industry":"int64"})
intensity_data.rename(columns = {"value_added":"output"}, inplace = True)

#import country codes as computed by our assignee#
assignee_imputed_data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/master_data_set_merged_imputed_efid_assignee_second_loop.csv", usecols = ["appln_psn_id", "imputed_country_efid_second_loop"], dtype = {"imputed_country_efid_second_loop":"category"})

#import the Technology Field data from the DB query using the "Canadian Innovations" efids#
techn_field_data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Canadian_inventions_from_assignee_imputation_techn_field_data_only.csv", index_col = 0)

#import techn_field lookup table
techn_field_lookup_table = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/Techn_Field_Table.csv", usecols = ["techn_field_nr", "techn_field"])
techn_field_lookup_table.drop_duplicates(inplace = True)

#import grant_date data
publn_grant_data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/publication_table_data.csv", index_col = 0)
publn_grant_data["publn_date"] = pd.to_datetime(publn_grant_data["publn_date"])

#prove there are no duplicates for this set
publn_grant_data[publn_grant_data.duplicated("appln_id")]


######################################
##Convert dates to date-time objects##
######################################
def dateConverter(df):
    df["appln_filing_date"] = pd.to_datetime(df["appln_filing_date"])
    df["earliest_filing_date"] = pd.to_datetime(df["earliest_filing_date"])
    return df

base_data = dateConverter(base_data)

##############################################
##Create the appln_psn_id from the base data##
##############################################
def applnpsnidCreator(df):
    df["appln_psn_id"] = df.appln_id.astype("str") + df.psn_id.astype("str")
    df["appln_psn_id"] = df["appln_psn_id"].astype("int64")
    return df

base_data = applnpsnidCreator(base_data)

##########################################
##Add in the imputed country code column##
##########################################

base_data = pd.merge(left = base_data, right = assignee_imputed_data, on = "appln_psn_id", how = "left")


########################################################
##Parse NACE2 Codes, merge intensity onto parsed codes##
########################################################
def industry2DigitCreator(df):
    #Shorten ISIC codes to create a nace industry code
    df["industry"] = df.apply(lambda x: int(str(x["nace2_code"]).split(".")[0]), axis = 1)
    return df

NACE_data = industry2DigitCreator(NACE_data)

#Merge the industry data from R&D intensity onto NACE
NACE_data_merged = pd.merge(left = NACE_data, right = intensity_data[["industry", "intensity"]], on = "industry", how = "left")


############################
##Create Intensity Measure##
############################

def intensityMeasureCreation(df):

    #make one intensity metric per application
    df["weighted_intensity"] =  df["weight"]*df["intensity"]

    #merged the sum of the weighted intensity measure per applicaiton onto the data
    NACE_appln_int = pd.merge(left = df, right = df.groupby("appln_id")["weighted_intensity"].sum(skipna = False).reset_index().rename(columns = {"weighted_intensity":"application_intensity"}), on = "appln_id", how = "left")

    #average the application intensity over the innovation to get the innovation intensity
    NACE_efid_int = pd.merge(left = NACE_appln_int, right = NACE_appln_int.groupby("earliest_filing_id")["application_intensity"].mean().reset_index().rename(columns = {"application_intensity":"innovation_intensity"}), on = "earliest_filing_id", how = "left")

    """
    For instances with intensity measure of NaN, the NaN is being weighted into the mean() function and skipna = False
    does not work without tossing an error. To solve, find intensity = NaN and rewrite applicaiton intensity
    and innovation intensity as NaNs.
    """

    #Find the applications and innovations with NaN intensity
    applications_nan_intensity = NACE_efid_int[NACE_efid_int["intensity"].isna()]["appln_id"].unique()
    innovations_nan_intensity = NACE_efid_int[NACE_efid_int["intensity"].isna()]["earliest_filing_id"].unique()

    #Rewrite those application and innovation intensities as NaNs
    NACE_efid_int.at[NACE_efid_int[NACE_efid_int["appln_id"].isin(applications_nan_intensity)].index, "application_intensity"] = np.nan
    NACE_efid_int.at[NACE_efid_int[NACE_efid_int["earliest_filing_id"].isin(innovations_nan_intensity)].index, "innovation_intensity"] = np.nan

    return NACE_efid_int

NACE_efid_int = intensityMeasureCreation(NACE_data_merged)

#Look at the percentage of lost patents from the whole data set due to NaNs from the intensity.
len(set(NACE_efid_int["appln_id"]) - set(NACE_efid_int[NACE_efid_int["innovation_intensity"].notnull()]["appln_id"])) / len(base_data["appln_id"].unique())
len(set(NACE_efid_int["earliest_filing_id"]) - set(NACE_efid_int[NACE_efid_int["innovation_intensity"].notnull()]["earliest_filing_id"])) / len(base_data["earliest_filing_id"].unique())

#####################################
##Weighted Dummyize the NACE Fields##
#####################################

def dummyizeNACEFields(df):
    #merged the names onto the
    NACE_full_desc = pd.merge(left = df, right = NACE_names, on = "nace2_code", how = "left")

    dummyize = pd.DataFrame(NACE_full_desc.groupby(["appln_id", "nace2_descr"])["weight"].sum()).reset_index().pivot(index = "appln_id" , columns = "nace2_descr")
    dummyize.columns = dummyize.columns.droplevel(0)
    dummyize = dummyize.reset_index()
    dummyize = dummyize.fillna(int(0))

    return dummyize


#merge the dummyized data onto the set
NACE_data = pd.merge(left = NACE_efid_int, right = dummyizeNACEFields(NACE_efid_int), on = "appln_id", how = "left")



#####################################
##Weighted Dummyize the Tech Fields##
#####################################

tech_field_data_matched = pd.merge(left = techn_field_data, right = techn_field_lookup_table, on = "techn_field_nr", how = "left")

def dummyizeTechnFields(df):
    dummyize_tech = pd.DataFrame(df.groupby(["appln_id", "techn_field"])["weight"].sum()).reset_index().pivot(index = "appln_id" , columns = "techn_field")
    dummyize_tech.columns = dummyize_tech.columns.droplevel(0)
    dummyize_tech = dummyize_tech.reset_index()
    dummyize_tech = dummyize_tech.fillna(int(0))

    return dummyize_tech




#merge the dummyized data onto the set
NACE_data = pd.merge(left = NACE_data, right = dummyizeTechnFields(tech_field_data_matched), on = "appln_id", how = "left")


#############################
##Publication Data Merge On##
#############################


NACE_data = pd.merge(left = NACE_data, right = publn_grant_data[["appln_id", "publn_first_grant", "publn_date"]], on = "appln_id", how = "left")



###################################################
##subset to only the ownership type onto the data##
###################################################

def dataReductionToOwnership(base_data, NACE_data):
    #create a list of ids where there is no priority applicant listed
    unknown_applicants =  set(base_data["appln_id"]) - set(base_data[base_data["applt_seq_nr"] == 1]["appln_id"])
    #create a sub df from the list of applications with no priority listed applicant
    no_applicant =  base_data.set_index("appln_id").loc[unknown_applicants]

    #append the priority and non-priority listings together
    data_with_owner_type = base_data[base_data["applt_seq_nr"] == 1].append(no_applicant[no_applicant["invt_seq_nr"] == 1].reset_index())



    data_with_NACE = pd.merge(left = data_with_owner_type, right = NACE_data.drop(columns = "earliest_filing_id"), on = "appln_id", how = "left")

    data_with_NACE = data_with_NACE.drop(columns = ["nace2_code", "weight", "industry", "intensity", "weighted_intensity"]).drop_duplicates()


    data_with_NACE_clean = data_with_NACE.dropna(subset = list(NACE_names["nace2_descr"]))

    data_with_NACE_clean["psn_sector"] = data_with_NACE_clean["psn_sector"].replace(["GOV NON-PROFIT UNIVERSITY", "COMPANY GOV NON-PROFIT", "COMPANY HOSPITAL", "COMPANY UNIVERSITY", "HOSPITAL", "GOV NON-PROFIT"], "OTHER").fillna("UNKNOWN")

    return data_with_NACE_clean


data_with_NACE_clean = dataReductionToOwnership(base_data, NACE_data)


###################################
#Create Canada Exclusive Variable##
###################################

def createCanadaExclusiveVariable(data_with_NACE_clean):

    appln_weights = pd.DataFrame(data_with_NACE_clean.groupby("earliest_filing_id")["appln_auth"].value_counts(normalize = True)).rename(columns = {"appln_auth":"CA_Exclusive"}).reset_index()
    CA_exclusives = appln_weights[(appln_weights["appln_auth"] == "CA") & (appln_weights["CA_Exclusive"] == 1)].drop(columns = "appln_auth")

    data_with_NACE_clean = pd.merge(left = data_with_NACE_clean, right = CA_exclusives, on = "earliest_filing_id", how = "left")

    #Create the exclusive column
    data_with_NACE_clean["CA_Exclusive"] = data_with_NACE_clean["CA_Exclusive"].fillna(0).astype("int64")

    return data_with_NACE_clean


data_with_NACE_clean = createCanadaExclusiveVariable(data_with_NACE_clean)

#new columns start at Analysis of biological materials
data_with_NACE_clean.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/master_data_set_canadian_innovations_for_regression_analysis.csv", index = False, encoding = "utf-8")
