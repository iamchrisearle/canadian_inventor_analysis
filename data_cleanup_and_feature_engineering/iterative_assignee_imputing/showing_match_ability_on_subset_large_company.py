##############################
##Assignee Matcher on Subset##
##############################

"""
Takes in a null and not-null SUBSET with the top x amount of assignees removed.
SUBSET is created from the subsetting_data_by_high_employee_count_assignees.py file
Does the assignee match on these inventions.
highly matched inventors file is exported
"""



import pandas as pd
import dask.dataframe as dd
from fuzzywuzzy import fuzz
import time
import os


#setup use columns to reduce data size
not_null_cols = ["earliest_filing_id","appln_filing_date", "appln_id", "psn_name", "person_ctry_code", "assignee_id", "imputed_country_efid"]
nul_cols = ["earliest_filing_id", "appln_filing_date", "appln_id", "psn_name", "person_ctry_code", "appln_psn_id", "assignee_id", "imputed_country_efid"]
#dtype dict declaration
dtypeDict = {"appln_id":"int64", "earliest_filing_id":"int64", "psn_id":"int64", "applt_seq_nr":"int64", "invt_seq_nr":"int64", "assignee_id":"int64", "appln_psn_id":"int64", "person_ctry_code":"object", "imputed_country_efid":"object"}
#bring in data files. Set null as the base and match again not-null in chunks
null_country = pd.read_csv("subset_top_5000_removed_null_inventor_only.csv", usecols = nul_cols,low_memory=False, dtype=dtypeDict)
not_null_country = pd.read_csv("subset_top_5000_removed_not_null.csv", chunksize = 1000000, low_memory=False, usecols = not_null_cols, dtype=dtypeDict)

null_country.set_index("assignee_id", inplace=True)
null_country.sort_index(inplace = True)
null_country = dd.from_pandas(null_country, npartitions = ncpus)

def fuzzMatcher(x, y):
    return fuzz.token_set_ratio(x, y)

def highlyMatchedFinder(not_null_country, null_country):
    counter =  0
    total_time = 0
    #print("partitioning")
    #not_null_chunk = dd.from_pandas(test_company_not_null, npartitions = 12)
    #null_chunk = dd.from_pandas(test_company_null, npartitions = 12)
    start = time.time()
    for chunk in not_null_country:
        chunk.set_index("assignee_id", inplace = True)
        chunk.sort_index(inplace = True)
        not_null_chunk = dd.from_pandas(chunk, npartitions = ncpus)
        #pair up the names on the same invention
        print("merging")
        workingMatchDF = null_chunk.join(not_null_country, on = "assignee_id", how = "inner", lsuffix='_x', rsuffix='_y').compute(scheduler = "processes")
        workingMatchDF = dd.from_pandas(workingMatchDF.reset_index(), npartitions = ncpus)
        #fuzz match all the pairwise names
        print("fuzzy matching")
        workingMatchDF["fuzz"] = workingMatchDF.map_partitions(lambda df: df.apply(lambda row: fuzzMatcher(row.psn_name_x, row.psn_name_y), axis = 1)).compute(scheduler = "processes")
        print("computing")
        highlyMatchedDF = workingMatchDF[workingMatchDF["fuzz"] > 90].compute()
        print("exporting")
        with open("assignee_matching/highly_matched_df_from_assignee_subset_" + str(counter) + ".csv", "a", encoding = "utf-8") as f:
            highlyMatchedDF.to_csv(f, header=f.tell()==0, encoding = "utf-8")
        duration = time.time() - start
        total_time += duration
        counter += 1
        print(f"Chunk of {len(not_null_chunk)} finished matching {len(workingMatchDF)} rows in {duration} seconds")
        del workingMatchDF
        del highlyMatchedDF
    print(f"Process took {total_time} seconds")
    return

highlyMatchedFinder(not_null_country, null_country)


with open("List_of_finished_assignee_ids.txt", "w+", encoding = "utf-8") as file:
    for id in set(null_country["assignee_id"]).union(set(not_null_country["assignee_id"])):
        file.write(str(id) + ",")
