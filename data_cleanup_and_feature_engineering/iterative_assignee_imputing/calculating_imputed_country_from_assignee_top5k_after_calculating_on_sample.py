"""
Takes in the same top 5k subsets subset for both null and not-null
Creates a drop list from the List_of_finished_assignee_ids which comes from both
The calculating_imputed_country_from_assignee_top5k_sample_under_25m_rows.py file
and the showing_match_ability_on_subset_large_company.py file
matches up by company in chunks and puts the files out into a folder

!!!
This file takes a very long time to run.
You can change the speed by augmenting the chunk-size but larger chunks are more likely to crash
!!!
"""


import pandas as pd
import dask.dataframe as dd
from fuzzywuzzy import fuzz
import time
import os
import numpy as np
from dask.distributed import Client
client = Client()
client.scatter
client

nul_cols = ["earliest_filing_id", "appln_filing_date", "appln_id", "psn_name", "person_ctry_code", "appln_psn_id", "assignee_id", "imputed_country_efid"]
not_null_cols = ["earliest_filing_id","appln_filing_date", "appln_id", "psn_name", "person_ctry_code", "assignee_id", "imputed_country_efid"]

dtypeDict = {"appln_id":"int64", "earliest_filing_id":"int64", "psn_id":"int64", "applt_seq_nr":"int64", "invt_seq_nr":"int64", "assignee_id":"int64", "appln_psn_id":"int64", "person_ctry_code":"object", "imputed_country_efid":"object"}

null_country = pd.read_csv("subset_top_5000_ONLY_null_inventor_only.csv", usecols = nul_cols, dtype=dtypeDict, low_memory = False)
not_null_country = pd.read_csv("subset_top_5000_ONLY_not_null.csv",  usecols = not_null_cols, dtype=dtypeDict, low_memory = False)

not_null_country.drop_duplicates(["psn_name", "appln_filing_date", "imputed_country_efid"], keep="first", inplace = True)



with open("List_of_finished_assignee_ids.txt", "r+") as file:
    drop_list = file.readlines()
    drop_list = drop_list[0].split(",")
    drop_list = [int(i) for i in drop_list]


#####################################
##Pick up here if above is complete##
#####################################

keep_list = list(set(null_country["assignee_id"].unique()) - set(drop_list))

#this is for hopping back in if it crashes
with open("completed_large_companies.txt") as file:
    finished_IDs = file.readlines()
    finished_IDs = finished_IDs[0].split(",")
    finished_IDs = [int(i) for i in finished_IDs]

keep_list = set(keep_list).difference(set(finished_IDs))

def matchOrdering():
    sortList = pd.DataFrame()
    for company in keep_list:
        sortList = sortList.append(pd.DataFrame([company, len(null_country[null_country["assignee_id"] == company])*len(not_null_country[not_null_country["assignee_id"] == company])]).T)
    return sortList

sortList = matchOrdering()
keep_list = sortList.sort_values(by=1)[0]
###
#Use this one to run the ones the super computer dumped
#too big, has been place onto the crash_list file.18631815
keep_list = [29726799]

#since returning from vacation


#cant find 111650821

#This one did 17 out of 18 iteration


def fuzzMatcher(x, y):
    return fuzz.token_set_ratio(x, y)


def iterativeCompanyChunkParse(not_null_country, null_country):
    start = time.time()
    counter =  1
    total_time = 0
    print("subsetting df 1")
    null_country.set_index("assignee_id", inplace=True)
    null_country.sort_index(inplace = True)
    not_null_country.set_index("assignee_id", inplace = True)
    not_null_country.sort_index(inplace = True)
    for company in keep_list:
        print(f"working on company {company}")
        company_time = time.time()
        #subset out the company from the null
        #null_country = dd.from_pandas(null_country, npartitions = 4)
        null_country_df = null_country.loc[company].copy()
        #subset out the company from the non-null
        print("subsetting df2")
        not_null_country_df = not_null_country.loc[company].copy()
        #chunksize the not_null_country
        chunksize = 100
        iterations = len(not_null_country_df)/chunksize
        #create cap
        current_count = 0
        #set slicing parameters
        x = 0
        y = chunksize
        #loop and chunk
        while current_count < iterations:
            #splice out the df
            not_null_country_df_chunk = not_null_country_df[x:y]
            #convert to dask
            #not_null_country_df_chunk = dd.from_pandas(not_null_country_df_chunk, npartitions = 4)
            #merge process
            print(f"merging on iteration {current_count} of {iterations}. {len(null_country_df)} rows from df1. Total {len(null_country_df)*len(not_null_country_df_chunk)} rows")
            workingMatchDF = null_country_df.join(not_null_country_df_chunk, on = "assignee_id", how = "inner", lsuffix='_x', rsuffix='_y')#.compute(scheduler = "processes")
            workingMatchDF = dd.from_pandas(workingMatchDF.reset_index(), npartitions = 8)
            #fuzz process
            print("fuzzy matching")
            workingMatchDF["fuzz"] = workingMatchDF.map_partitions(lambda df: df.apply(lambda row: fuzzMatcher(row.psn_name_x, row.psn_name_y), axis = 1)).compute(scheduler = "processes")
            #workingMatchDF["fuzz"] = workingMatchDF.apply(lambda row: fuzzMatcher(row.psn_name_x, row.psn_name_y), axis = 1)
            print("computing")
            highlyMatchedDF = workingMatchDF[workingMatchDF["fuzz"] > 90].compute()
            print("exporting")
            with open("assignee_matching/highly_matched_df_from_assignee_subset_top_5k_" +  str(company) + "_" + str(current_count) + ".csv", "a", encoding = "utf-8") as f:
                highlyMatchedDF.to_csv(f, header=f.tell()==0, encoding = "utf-8")
            x += chunksize
            y += chunksize
            current_count += 1
            del highlyMatchedDF
            del workingMatchDF
        duration = time.time() - company_time
        print(f"Completed company {company} in {duration} seconds")
        with open("completed_large_companies.txt", "a+") as file:
            file.write("," + str(company))
    duration = time.time() - start
    print(f"Process took {duration} seconds")

iterativeCompanyChunkParse(not_null_country, null_country)
