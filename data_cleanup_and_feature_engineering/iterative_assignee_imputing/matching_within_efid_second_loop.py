###################################
####Calculates Imputed Country ####
###################################


"""
Takes in the dataframes from the various Data Quadrants as subsets out from
the preprocessing_for_same_invention_country_country_assigner.py file
Uses fuzzy matching on inventors within an invention to impute inventor countries from the assignee step
Drop duplicate name matches within an application (Can't be two people at once)
Reduces each appln_psn_id code down to the minimum days within that appln_psn_id
Drops duplicate countries within an appln_psn_id
Where different countries exist per appln_psn_id, randomly selects a country with seed = 1
"""


import pandas as pd
import dask.dataframe as dd
from fuzzywuzzy import fuzz
import time
import os
from dask.distributed import Client
client = Client()
client.scatter
client
pd.set_option("mode.chained_assignment", None)



null_country = pd.read_csv("D:/PATSTAT Result Data Sets/Second EFID Loop/any_null_country_inventor_only_from_assignee_results_use_for_second_EFID_loop.csv", low_memory=False, dtype={"appln_psn_id":"int64"})
not_null_country = pd.read_csv("D:/PATSTAT Result Data Sets/Second EFID Loop/NOT_null_country_inventor_only_from_assignee_results_use_for_second_EFID_loop.csv", chunksize=2000000, low_memory=False, dtype={"appln_psn_id":"int64"})



###With Dask###
def fuzzMatcher(x, y):
    return fuzz.token_set_ratio(x, y)

def highlyMatchedFinder(null_country, not_null_country):
    #create an empty df
    highlyMatchedDF = pd.DataFrame()
    #declare which cols to pull
    not_null_cols = ["earliest_filing_id", "appln_filing_date", "appln_id", "psn_name", "person_ctry_code", "imputed_country_efid_y"]
    nul_cols = ["earliest_filing_id", "appln_filing_date", "appln_id", "psn_name", "person_ctry_code", "appln_psn_id", "imputed_country_efid_y"]
    total_time = 0
    #cycle through the chunk imports
    for chunk in not_null_country:
        start = time.time()
        #pair up the names on the same invention
        print("merging...")
        workingMatchDF = pd.merge(right = chunk[not_null_cols], left = null_country[nul_cols], on = "earliest_filing_id")
        print("converting to dask")
        workingMatchDF = dd.from_pandas(workingMatchDF, npartitions = 12)
        #fuzz match all the pairwise names
        print("matching...")
        workingMatchDF["fuzz"] = workingMatchDF.map_partitions(lambda df: df.apply(lambda row: fuzzMatcher(row.psn_name_x, row.psn_name_y), axis = 1)).compute(scheduler = "processes")
        print("appending")
        highlyMatchedDF = highlyMatchedDF.append(workingMatchDF[workingMatchDF["fuzz"] > 90].compute())
        duration = time.time() - start
        total_time += duration
        print(f"Chunk of {len(chunk)} finished in {duration} seconds")
    print(f"Process took {total_time} seconds")
    return highlyMatchedDF



highlyMatched = highlyMatchedFinder(null_country, not_null_country)
highlyMatched.rename(columns = {"imputed_country_efid_y_y":"imputed_country_efid_second_loop"}, inplace = True)
print("Exporting...")
highlyMatched.reset_index().to_csv("D:/PATSTAT Result Data Sets/Second EFID Loop/highly_matched_df_from_second_loop_efid.csv")
