"""
Takes in the highly matched dataframe from the second loop efid file created in "matching_within_efid_second_loop.py"
Creates the days between variable
Drop duplicate name matches within an application (Can't be two people at once)
Reduces each appln_psn_id code down to the minimum days within that appln_psn_id
Drops duplicate countries within an appln_psn_id
Where different countries exist per appln_psn_id, randomly selects a country with seed = 1
"""


import pandas as pd
import dask.dataframe as dd



############################################
##Remove Duplicates and Find Earliest Date##
############################################

dtypeDict = {"appln_id_x" : "int64", "appln_id_y":"int64", "appln_psn_id": "int64", "assignee_id" : "int64", "earliest_filing_id_x" : "int64", "earliest_filing_id_y":"int64",
 "fuzz":"int64", "imputed_country_efid_x":"category", "imputed_country_efid_y":"object", "person_ctry_code_x":"category", "person_ctry_code_y":"category", "psn_name_x":"object", "psn_name_y":"object", "imputed_country_efid": "object"}

imputed_efid_second_loop = dd.read_csv("D:/PATSTAT Result Data Sets/Second EFID Loop/highly_matched_df_from_second_loop_efid.csv", dtype = dtypeDict, low_memory = False)
imputed_efid_second_loop.dtypes


##########################################
##Date Cleanup and Days Between Creation##
##########################################


def highlyMatchedDataCleanup(high_matched):
    #convert to datetime objects
    high_matched["appln_filing_date_x"]=dd.to_datetime(high_matched["appln_filing_date_x"], errors = "coerce")
    high_matched["appln_filing_date_y"]=dd.to_datetime(high_matched["appln_filing_date_y"], errors = "coerce")


    #calculate days between column
    high_matched['days'] = abs((high_matched['appln_filing_date_x'] - high_matched['appln_filing_date_y']).dt.days)

    #drop the unnamed column from import
    #high_matched = high_matched.drop("Unnamed: 0", axis = 1)

    #run the computation
    df = %time high_matched.compute()
    return df

df = highlyMatchedDataCleanup(imputed_efid_second_loop)


###############################
##Determining who to invclude##
###############################

def dropDuplicateNames(df):
    """
    Drops duplicate matches between a appln_psn_id and application y
    A person on a given application can't match to more than 1 person name on another application
    Because a person can't be two people per application at a time
    """
    #sort the matches by the highest fuzz match value.
    df2 = df.sort_values(by = ["appln_psn_id", "appln_id_y","fuzz"], ascending=False).copy()

    df2.drop_duplicates(["appln_psn_id", "appln_id_y"], inplace = True)
    #create a psn_id column
    df2["psn_id"] =  df2[["appln_id_x", "appln_psn_id"]].apply(lambda x: int(str(x["appln_psn_id"]).split(str(x["appln_id_x"]))[1]), axis = 1)
    return df2

df2  = dropDuplicateNames(df)


######################
##Clean Up and Merge##
######################

def dropNonMinDays(df2):
    """
    Finds the minimum days within an appln_psn_id.
    Returns only rows with the min days between.
    Removes duplicate country codes within the min days
    """
    #Return only the observations with the minimum number of days for a given appln_psn_id's matches
    min_days_df = df2[df2['days'] == df2.groupby(["appln_psn_id", "psn_name_x"])['days'].transform(min)]

    ###
    #If a country is returned more than once with the same number of days between, remove the duplicates
    ####I might change this process to allow for a higher chance of rolling a country which appears on multiple filings on the last day
    ####
    min_days_df_clean = min_days_df.drop_duplicates(["appln_psn_id", "imputed_country_efid_second_loop"])
    return min_days_df_clean


def cleanUpMultipleCountryMatches(df):
    """
    Takes any instance where the appln_psn_id returns only one country code, retains this df.
    Takes instances of mulitple country codes on the appln_psn_id level, randomonly  picks one country.
    Appends the randomly selected country df to the first retained df.
    """
    #first, just pull out the ones without dupliates
    df_no_duplicates = df.drop_duplicates("appln_psn_id", keep = False)


    #this looks at which inventors have duplicate minimum days
    check_list = list(df[df.duplicated("appln_psn_id", keep = False)]["appln_psn_id"].unique())
    min_days_df_clean_dups =  df[df.duplicated("appln_psn_id", keep = False)].set_index(["appln_psn_id", "psn_name_x"])


    #create a blank df for appending results of randomization from duplicate countries
    randomized_df = pd.DataFrame()

    #using the seed of 1, pull a sample from the available names
    for person in check_list:
        randomized_df = randomized_df.append(min_days_df_clean_dups.loc[person].sample(n = 1, random_state = 1))

    #rebuild the appln_psn_id variable which was lost in the step above
    randomized_df["appln_psn_id"] = randomized_df[["appln_id_x", "psn_id"]].apply(lambda x: int(str(x["appln_id_x"]) + str(x["psn_id"])), axis = 1)

    #append the non-duplicated and the duplicated-after-randomizing data sets together
    final_cleaned_set = randomized_df.reset_index().append(df_no_duplicates)
    return final_cleaned_set

final_cleaned_set = cleanUpMultipleCountryMatches(dropNonMinDays(df2))
final_cleaned_set.drop(columns = ["Unnamed: 0", "index"], inplace = True)

final_cleaned_set.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/imputed_assignee_results_by_appln_psn_id_and_country_code_from_second_loop.csv", index = False)
