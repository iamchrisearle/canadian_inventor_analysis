"""
Takes in the highly matched master dataframe and choses one country code per invetor per application
Multiple methods of doing this, so this will showcase several of them
"""


#import packages
import pandas as pd
import datetime
import dask.dataframe as dd
from dask.distributed import Client
client = Client()
client.scatter
client

################
##Data Cleanup##
################

#Bring in the data set and clear out null variables and invalid dates
#added in null variables of the default 9999 date and instances of the header appearing as a value
high_matched = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/master_highly_matched_data_set.csv", low_memory=True,  na_values = ["9999-12-31",  "appln_filing_date_x"], index_col = 0)

#remove all the null variables
high_matched = high_matched[high_matched["appln_filing_date_x"].notnull()]
list(high_matched.columns[1:])
#export as a clean data set
high_matched.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/master_highly_matched_data_set_clean.csv", columns = list(high_matched.columns[1:]), index = False)






##########################
import pandas as pd
import datetime
import os
import random
import dask.dataframe as dd
from dask.distributed import Client
client = Client()
client.scatter
client

pd.options.display.max_columns = None
pd.options.display.max_rows = 1500

os.getcwd()

dtypeDict = {"appln_id_x" : "int64", "appln_id_y":"int64", "appln_psn_id": "int64", "assignee_id" : "int64", "earliest_filing_id_x" : "int64", "earliest_filing_id_y":"int64",
 "fuzz":"int64", "imputed_country_efid_x":"category", "imputed_country_efid_y":"category", "person_ctry_code_x":"category", "person_ctry_code_y":"category", "psn_name_x":"object", "psn_name_y":"object"}

high_matched = dd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/master_highly_matched_data_set_clean.csv", low_memory=True, dtype=dtypeDict)
#high_matched = dd.from_pandas(high_matched, npartitions = 12)

high_matched.dtypes

#############################################
##Clean up dates and calculate days between##
#############################################


def highlyMatchedDataCleanup(high_matched):
    #convert to datetime objects
    high_matched["appln_filing_date_x"]=dd.to_datetime(high_matched["appln_filing_date_x"], errors = "coerce")
    high_matched["appln_filing_date_y"]=dd.to_datetime(high_matched["appln_filing_date_y"], errors = "coerce")


    #calculate days between column
    high_matched['days'] = abs((high_matched['appln_filing_date_x'] - high_matched['appln_filing_date_y']).dt.days)

    #drop the unnamed column from import
    #high_matched = high_matched.drop("Unnamed: 0", axis = 1)

    #run the computation
    df = %time high_matched.compute()
    return df

df = highlyMatchedDataCleanup(high_matched)
###############################
##Determining who to invclude##
###############################

def dropDuplicateNames(df):
    """
    Drops duplicate matches between a appln_psn_id and application y
    A person on a given application can't match to more than 1 person name on another application
    Because a person can't be two people per application at a time
    """
    #sort the matches by the highest fuzz match value.
    df2 = df.sort_values(by = ["appln_psn_id", "appln_id_y","fuzz"], ascending=False).copy()

    df2.drop_duplicates(["appln_psn_id", "appln_id_y"], inplace = True)
    #create a psn_id column
    df2["psn_id"] =  df2[["appln_id_x", "appln_psn_id"]].apply(lambda x: int(str(x["appln_psn_id"]).split(str(x["appln_id_x"]))[1]), axis = 1)
    return df2

df2  = dropDuplicateNames(df)

df2.dtypes

#Find the minimum number of days within an assignee and appln_psn_id then return those rows in a given assignee - appln_psn_id combo
%time min_days_df = df2[df2['days'] == df2.groupby(["assignee_id", "appln_psn_id"])['days'].transform(min)]


###
#If a country is returned more than once with the same number of days between, remove the duplicates
####I might change this process to allow for a higher chance of rolling a country which appears on multiple filings on the last day
####
min_days_df_clean = min_days_df.drop_duplicates(["appln_psn_id", "imputed_country_efid_y"])


######################
##Clean Up and Merge##
######################

#first, just pull out the ones without dupliates
min_days_df_clean_no_dup = min_days_df_clean.drop_duplicates("appln_psn_id", keep = False)


#this looks at which inventors have duplicate minimum days
check_list = list(min_days_df_clean[min_days_df_clean.duplicated("appln_psn_id", keep = False)]["appln_psn_id"].unique())
min_days_df_clean_dups =  min_days_df_clean[min_days_df_clean.duplicated("appln_psn_id", keep = False)].set_index(["appln_psn_id", "psn_name_x"])


#create a blank df for appending results of randomization from duplicate countries
randomized_df = pd.DataFrame()

#using the seed of 1, pull a sample from the available names
for person in check_list:
    randomized_df = randomized_df.append(min_days_df_clean_dups.loc[person].sample(n = 1, random_state = 1))

#rebuild the appln_psn_id variable which was lost in the step above
test = pd.DataFrame()
randomized_df["appln_psn_id"] = randomized_df[["appln_id_x", "psn_id"]].apply(lambda x: int(str(x["appln_id_x"]) + str(x["psn_id"])), axis = 1)

#append the non-duplicated and the duplicated-after-randomizing data sets together
final_cleaned_set = randomized_df.reset_index().append(min_days_df_clean_no_dup)

final_cleaned_set.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/imputed_assignee_results_by_appln_psn_id_and_country_code.csv")













##############################
##Quick stats on the results##
##############################

#How many of the newly assigned inventors have the same earliest filing id still?
#From the efid step, we didn't make a decision on results with multiple countries on the first day.
#Therefore, how many would have found anyways?
print(str((len(final_cleaned_set[final_cleaned_set["earliest_filing_id_x"] == final_cleaned_set["earliest_filing_id_y"]]) / len(final_cleaned_set))*100) + "%")

#how many new Canadians did this result in?
final_cleaned_set[final_cleaned_set["imputed_country_efid_y"] == "CA"]
len(final_cleaned_set[final_cleaned_set["imputed_country_efid_y"] == "CA"])
#how many new Canadians did we find striclty from the assignee imputing?
#i.e. how many would have been found if we randomly selected a country in the duplicates scenario anyways?

final_cleaned_set[(final_cleaned_set["imputed_country_efid_y"] == "CA") & (final_cleaned_set["earliest_filing_id_x"] == final_cleaned_set["earliest_filing_id_y"])]
len(final_cleaned_set[(final_cleaned_set["imputed_country_efid_y"] == "CA") & (final_cleaned_set["earliest_filing_id_x"] == final_cleaned_set["earliest_filing_id_y"])])
