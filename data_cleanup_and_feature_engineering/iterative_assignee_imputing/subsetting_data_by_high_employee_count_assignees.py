"""
When the Assignee data is too larget to match, use this file.
It will order the datafile by number of rows each assignee is taking up.
Use the keep variable to remove the assignees past the integer in the brackets.
highestCounts[5000:] will remove the top 5000 assignees.
"""

import pandas as pd
import dask.dataframe as dd
from fuzzywuzzy import fuzz
from dask.distributed import Client
client = Client()
client

pd.options.display.max_columns = None
pd.options.display.max_rows = 500


#Perform the reduction on the merged master DF
data = dd.read_csv("~/data_cleanup_and_feature_engineering/master_data_set_merged_imputed_efid_assignee.csv", low_memory = False, dtype={'days_between': 'object'})
data.head()
#value count on the ids
highestCounts = data["assignee_id"].value_counts().compute()
#take the top value counts
keep = highestCounts[5000:].index
#
reduced = data[data["assignee_id"].isin(keep)].compute()

reducedDask = dd.from_pandas(reduced, 20)

null_country = reducedDask[(reducedDask["imputed_country_efid").isnull() & (reducedDask["invt_seq_nr"] > 0)]].compute()

not_null_country = reducedDask[reducedDask["imputed_country_efid"].notnull()].compute()

null_country.to_csv("subset_top_5000_removed_null_inventor_only.csv")
not_null_country.to_csv("subset_top_5000_removed_not_null.csv")



keep = highestCounts[0:5000].index
#
reduced = data[data["assignee_id"].isin(keep)].compute()

reducedDask = dd.from_pandas(reduced, 20)

null_country = reducedDask[(reducedDask["imputed_country_efid").isnull() & (reducedDask["invt_seq_nr"] > 0)]].compute()

not_null_country = reducedDask[reducedDask["imputed_country_efid"].notnull()].compute()

null_country.to_csv("subset_top_5000_ONLY_null_inventor_only.csv")
not_null_country.to_csv("subset_top_5000_ONLY_not_null.csv")
