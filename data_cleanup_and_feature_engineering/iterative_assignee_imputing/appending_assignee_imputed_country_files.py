"""
Reads in the pieces of highly  matched inventors from various process and glues the csvs together to make the master highly matched csv
CSVs being glued:
Highly matched on ~5m assignees from local calculation
Highly matched on large companies with <25m rows to match from local
Highly matched on large companies from the super computer calculations
"""


import pandas as pd
import os

pd.options.display.max_columns = None
pd.options.display.max_rows = 150


path = "D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/"
files_list = os.listdir(path)
files_list.remove("Remaining Large Companies")

highly_matched = pd.DataFrame()

#From the top 5k, under 25m rows
for file in files_list[0:2]:
    df = pd.read_csv(path + file)
    highly_matched = highly_matched.append(df)
    print(f"Finished {file}")


highly_matched2 = pd.DataFrame()

#From the first 5 mill
path = "D:/PATSTAT Result Data Sets/Final Data Sets/AssigneeMatchDump/"
files_list = os.listdir(path)

for file in files_list:
    df = pd.read_csv(path + file)
    highly_matched2 = highly_matched2.append(df)
    print(f"Finished {file}")


highly_matched.head()
highly_matched2.head()

highly_matched.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/highly_matched_from_top_5k_under_25m_row_match.csv")
highly_matched2.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/highly_matched_from_under_top_5k.csv")

dtypeDict = {"assignee_id":"int64", "appln_id_x":"int64", "person_ctry_code_x":"object", "person_ctry_code_y":"object", "imputed_country_efid_y":"object", "imputed_country_efid_x":"object"}


highly_matched = pd.read_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/highly_matched_from_top_5k_under_25m_row_match.csv", dtype=dtypeDict)
highly_matched2 = pd.read_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/highly_matched_from_under_top_5k.csv", dtype=dtypeDict)

highly_matched.rename(columns = {"appln_id_x":"appln_id_y", "appln_id_y":"appln_id_x", "appln_filing_date_x":"appln_filing_date_y", "appln_filing_date_y":"appln_filing_date_x",
"earliest_filing_id_x":"earliest_filing_id_y", "earliest_filing_id_y":"earliest_filing_id_x", "person_ctry_code_x":"person_ctry_code_y", "person_ctry_code_y":"person_ctry_code_x",
"psn_name_x":"psn_name_y", "psn_name_y":"psn_name_x", "imputed_country_efid_x":"imputed_country_efid_y", "imputed_country_efid_y":"imputed_country_efid_x"}, inplace = True)



#looks like I joined them backwards between the two files.
#No problem - just change imputed_country_efid_x to y on one of them


highly_matched3 = highly_matched.append(highly_matched2, sort = True)

highly_matched3.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/highly_matched_under_top_5k_AND_under_25m_rows.csv")




##############################
#Append from highly Top 5k remaining large companies
path = "D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/Remaining Large Companies/"
files_list = os.listdir(path)
files_list.remove("Remaining Large Companies")

highly_matched = pd.DataFrame()


#From the top 5k, under 25m rows
#First add 100 csvs at a time to reduce appending memory
counter = 0
iteration = 0
for file in files_list:
    df = pd.read_csv(path + file, index_col=0)
    highly_matched = highly_matched.append(df)
    highly_matched.drop_duplicates()
    counter += 1
    del df
    if counter == 100:
        highly_matched.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/DF Glue/highly_matched_iteration_" + str(iteration) + ".csv")
        iteration += 1
        counter = 0
        highly_matched = pd.DataFrame()
    print(f"Finished {file}, {counter}")
highly_matched.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/DF Glue/highly_matched_iteration_" + str(iteration+1) + ".csv")


#Then take those and stick append them
files_list = os.listdir("D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/DF Glue/")
highly_matched = pd.DataFrame()

counter = 0
iteration = 0
for file in files_list:
    df = pd.read_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/DF Glue/" + file, index_col=0)
    highly_matched = highly_matched.append(df)
    highly_matched.drop_duplicates(inplace = True)
    counter += 1
    del df
    print(f"Finished {file}, {counter}")
highly_matched.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/DF Glue/highly_matched_final_iteration.csv")


##############################
#Append from Top 5k super computer
#first iteration
path = "D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/From Super Computer/"
files_list = os.listdir(path)


highly_matched = pd.DataFrame()


#first iteration from super computer
counter = 0
iterations = 0
for file in files_list:
    df = pd.read_csv(path + file, index_col=0)
    highly_matched = highly_matched.append(df)
    highly_matched.drop_duplicates()
    del df
    counter += 1
    if counter == 25:
        highly_matched.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/DF Glue/From Super Computer/highly_matched_" + str(iterations) + ".csv")
        iterations += 1
        counter = 0
        highly_matched = pd.DataFrame()
    print(f"Finished {file}, {counter}")
highly_matched.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/DF Glue/From Super Computer/highly_matched_" + str(iterations+1) + ".csv")

#second iteration from super computer
path = "D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/DF Glue/From Super Computer/"
files_list = os.listdir(path)


highly_matched = pd.DataFrame()
counter = 0
iteration = 0

for file in files_list:
    df = pd.read_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/DF Glue/From Super Computer/" + file, index_col=0)
    highly_matched = highly_matched.append(df)
    highly_matched.drop_duplicates(inplace = True)
    counter += 1
    del df
    print(f"Finished {file}, {counter}")

highly_matched.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/DF Glue/From Super Computer/highly_matched_final_iteration.csv")









#############################
#Glueing everything together#
#############################

highly_matched_small = pd.read_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/highly_matched_under_top_5k_AND_under_25m_rows.csv")
highly_matched_large_local = pd.read_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/DF Glue/highly_matched_final_iteration.csv", index_col = 0)
highly_matched_large_supercomputer = pd.read_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/Top5k/DF Glue/From Super Computer/highly_matched_final_iteration.csv", index_col = 0)

highly_matched_small.head()
highly_matched_large_local.head()
highly_matched_large_supercomputer.head()

highly_matched_small.drop(columns = list(highly_matched_small.columns[0:3]), inplace = True)


highly_matched_small.rename(columns = {"appln_id_x":"appln_id_y", "appln_id_y":"appln_id_x", "appln_filing_date_x":"appln_filing_date_y", "appln_filing_date_y":"appln_filing_date_x",
"earliest_filing_id_x":"earliest_filing_id_y", "earliest_filing_id_y":"earliest_filing_id_x", "person_ctry_code_x":"person_ctry_code_y", "person_ctry_code_y":"person_ctry_code_x",
"psn_name_x":"psn_name_y", "psn_name_y":"psn_name_x", "imputed_country_efid_x":"imputed_country_efid_y", "imputed_country_efid_y":"imputed_country_efid_x"}, inplace = True)



highly_matched_append_one = highly_matched_small.append(highly_matched_large_local, sort = True)
del highly_matched_small
del highly_matched_large_local


highly_matched_append_two = highly_matched_append_one.append(highly_matched_large_supercomputer, sort = True)
del highly_matched_append_one

#export the master file after all others have been appended
highly_matched_append_two.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/master_highly_matched_data_set.csv")
