"""
This is a weird file - it's in various steps and only certain parts are run in certain steps.
There are no functions as it's better to run it in sections, line by line to see the results at each step and ensure correctness.
Writes out a lot of large data files, requires a good amount of storage and memory
"""


import pandas as pd
import numpy as np
import dask.dataframe as dd
import datetime
from dask.distributed import Client
client = Client()
client
pd.options.display.max_columns = None
pd.options.display.max_rows = 150

##############################################
##Use this section for imputing EFID country##
##############################################


#bring in the full data set with assignees and indexers
dtypeDict = {"appln_id":"int64", "earliest_filing_id":"int64", "psn_id":"int64", "applt_seq_nr":"int64", "invt_seq_nr":"int64", "assignee_id":"int64", "appln_psn_id":"int64"}
data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/allInventors2001-2016WithPSN_with_Indexer_and_Assignee_ID.csv", dtype = dtypeDict, low_memory=False)
#data.drop(columns = "Unnamed: 0", inplace = True)

#bring in the imputed country code data
imputed_country_efid = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/imputed_countries_from_graham_efid_method.csv", dtype = dtypeDict, low_memory=False, usecols=["appln_psn_id", "person_ctry_code_x"])
imputed_country_efid = imputed_country_efid.rename(columns = {"person_ctry_code_x" : "imputed_country_efid"})

#set indicies to do the join
data.set_index("appln_psn_id", inplace = True)
imputed_country_efid.set_index("appln_psn_id", inplace = True)

#join the imputed efids onto the master dataset
data_with_imputed_efid = data.join(imputed_country_efid, on = "appln_psn_id", how = "outer")

#fill the NAs with the data given from PATSTAT
data_with_imputed_efid["imputed_country_efid"] = data_with_imputed_efid["imputed_country_efid"].fillna(data_with_imputed_efid["person_ctry_code"])

#Export the merged file to be used in creating the assignee.
data_with_imputed_efid.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/data_master_with_merged_imputed_efid.csv")


########################################################
###hop back in once the assignee matching is complete###
########################################################

dtypeDict = {"appln_id_x" : "int64", "appln_id_y":"int64", "appln_psn_id": "int64", "assignee_id" : "int64", "earliest_filing_id_x" : "int64", "earliest_filing_id_y":"int64",
 "fuzz":"int64", "imputed_country_efid_x":"category", "imputed_country_efid_y":"object", "person_ctry_code_x":"category", "person_ctry_code_y":"category", "psn_name_x":"object", "psn_name_y":"object", "imputed_country_efid": "object"}

cols = ['appln_psn_id', 'appln_id', 'appln_auth', 'appln_filing_date',
       'earliest_filing_id', 'person_address', 'person_ctry_code', 'psn_id',
       'psn_name', 'applt_seq_nr', 'invt_seq_nr', 'assignee_id',
       'imputed_country_efid']

data_with_imputed_efid = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/data_master_with_merged_imputed_efid.csv", dtype = dtypeDict,  usecols = cols)
data_with_imputed_efid.set_index("appln_psn_id", inplace = True)
data_with_imputed_efid.dtypes

#bring in the imputed assignees df
imputed_country_assignee = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/imputed_assignee_results_by_appln_psn_id_and_country_code.csv", dtype = dtypeDict, low_memory = False, usecols = ["appln_psn_id", "imputed_country_efid_y"])
imputed_country_assignee.set_index("appln_psn_id", inplace = True)


#join the data set
%time data_with_imputed_efid_assignee = data_with_imputed_efid.join(imputed_country_assignee, on = "appln_psn_id", how = "left")
data_with_imputed_efid_assignee["imputed_country_efid_y"] = data_with_imputed_efid_assignee["imputed_country_efid_y"].fillna(data_with_imputed_efid["imputed_country_efid"])


data_with_imputed_efid_assignee.dtypes


#export the DF
data_with_imputed_efid_assignee.to_csv("D:/PATSTAT Result Data Sets/master_data_set_merged_imputed_efid_assignee.csv")




##########################################
##Hop back in after the efid second loop##
##########################################

dtypeDict = {"appln_id_x" : "int64", "appln_id_y":"int64", "appln_psn_id": "int64", "assignee_id" : "int64", "earliest_filing_id_x" : "int64", "earliest_filing_id_y":"int64",
 "fuzz":"int64", "imputed_country_efid_x":"category", "imputed_country_efid_y":"object", "person_ctry_code_x":"category", "person_ctry_code_y":"category", "psn_name_x":"object", "psn_name_y":"object", "imputed_country_efid": "object"}

cols = ['appln_psn_id', 'appln_id', 'appln_auth', 'appln_filing_date',
       'earliest_filing_id', 'person_address', 'person_ctry_code', 'psn_id',
       'psn_name', 'applt_seq_nr', 'invt_seq_nr', 'assignee_id',
       'imputed_country_efid', 'imputed_country_efid_y']

#read in the master data set from previous steps
data_with_imputed_assignee = pd.read_csv("D:/PATSTAT Result Data Sets/master_data_set_merged_imputed_efid_assignee.csv", dtype = dtypeDict,  usecols = cols)
data_with_imputed_assignee.set_index("appln_psn_id", inplace =True)
data_with_imputed_assignee.dtypes

#read in the results from the cleaned up highly matched data set from the second EFID loop
imputed_efid_second_loop = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/imputed_assignee_results_by_appln_psn_id_and_country_code_from_second_loop.csv", dtype = dtypeDict, low_memory = False)
imputed_efid_second_loop.set_index("appln_psn_id", inplace = True)

#Join the new highly matched results onto the master data set
%time data_with_imputed_efid_assignee_second_loop = data_with_imputed_assignee.join(imputed_efid_second_loop["imputed_country_efid_second_loop"], on = "appln_psn_id", how = "left")

#fill in the NaNs from the efid second loop results with those from the efid_y (which is the assignee imputation before the second loop)
data_with_imputed_efid_assignee_second_loop["imputed_country_efid_second_loop"] = data_with_imputed_efid_assignee_second_loop["imputed_country_efid_second_loop"].fillna(data_with_imputed_efid_assignee_second_loop["imputed_country_efid_y"])
data_with_imputed_efid_assignee_second_loop.reset_index(inplace = True)

#export the file
data_with_imputed_efid_assignee_second_loop.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/master_data_set_merged_imputed_efid_assignee_second_loop.csv", index = False)
