﻿# "Where do Canadians Patent? Implications for Canada's Optimal Patent Regime" Data Analysis

This code is to reproduce the data analysis undertaken by Joel Blit and Chris Earle.

Python scrips and some smaller CSVs are included. For larger CSV files contact Chris: ccearle@edu.uwaterloo.ca

The analysis is performed on the  PATSTAT-Spring-2019 database which has been purchase by us and is hosted at the University of Waterloo.
Anyone recreating the research will need access to the PATSTAT database which is available for purchase at:

https://www.epo.org/searching-for-patents/business/patstat.html#tab-1

We will not be hosting the dataset publically here. Recreation of this work will require full access to the PATSTAT database.

Our analysis utilizes patent and inventor data for every available filing between March 2001 and January 2017.
Because of the size of the data, we utilized the Compute Canada distributed computing system:

https://www.computecanada.ca/


The code shown here generalizes some of the processes used in our computations as calculations had to be broken down into chunks
or were distributed over a cluster. This code will run easily on a subset of the data, however, recreating our research in full will require 
substantial computing power and some processes may take several days or weeks to complete even with substantial computing power availability. 

## Prerequisites

Analysis is in Python 3.6.7 and requires the following up-to-date packages and their dependencies:
- pandas
- dask
- paramiko
- sshtunnel
- pymysql
- seaborn
- matplotlib
- fuzzywuzzy


## Data Collection Notes

The script which connects to the PATSTAT database requires .pem files containing login details to access the database.
Recreation will require adjustments which will allow the user access to their hosted database.



## Process Outline

Our data analysis follows this outline

Query through the DB to get all the filings for Mar2001 – 2016:
-	Use file “query_database_for_all_inventorsMar2001-2016.py”
-	This will be computationally expensive and likely tap out ram below 120GB 
-	Can change the query in the file to do so by year and piece them together later
-	A datafile has been saved for this after piecing together 16 queries

Create the Assignee for both the full data set and the “Canadians”
-	Use file “assignee_matching_on_full_data_and_canadians.py”
-	The function uses conditional masks to merge the assignee onto each invention
-	Utilizes the harmonized “psn_name” variable from PATSTAT
-	THIS PROCESS REDUCES THE COLUMNS. If other columns are required, must be declared on data import. 

Determine the extent of the missing data problem and preprocess for fuzzy matching:
-	Use file “preprocessing_for_same_invention_country_assigning.py”
-	Set a custom indexer column using a merge of the appln_id and the psn_id so that changes can be made upon any future data set through the custom index.
-	Prints summary statistics used to generate the Data Quadrants framework
-	Graphs the worst offending missing country code data countries
-	Exports the data into files for Quadrant 3, Quadrant 1, and Quadrants 4&6 for fuzzy matching

Create a highly matched DF within the EFID method:
-	Use the file “matching_within_efid.py” to generate a highly matched dataframe or if using the super computer use “dask_for_efid_matching.py”
-	It performs a join on an earliest filing ID between known and unknown country codes
-	Next, if fuzzy matches the distance between all names within an earliest filing id
-	Then it takes only the highly matched (90+ match ratio) names and returns them in a df

Calculate an Imputed Country Code from Earliest Filing IDs:
-	Use file “calculating_imputed_country_code_from_graham_highly_matched_output.py” 
-	Uses the matching algorithm across inventions to obtain a closest filing where possible
-	Matching algorithm could be done with either the highest weighted, or the closest last application using a randomizer for dealing with tie breakers
-	I achieved a 68.71% change in countries under Data Quadrant 3 on the inventor-application pair level

Assign Imputed Country Codes from Earliest Filing ID method to data set:
-	Use “imputed_country_code_merging_to_master_data.py” file
-	Using just the first half of the file, bring in the imputed country codes list 
-	Merge the imputed country code column onto the new file using the appln_psn_id column as the key
-	Exports the file out for Assignee processing




Imputing County Codes from Assignee ID (iterative_assignee_imputing folder):
-	Ideally, this would all be done with one file, but the data exponentially explodes when working on matches within a company. I therefore ran a subsetter file first to separate the 5k most intensive rows using the file “subsetting_data_by_high_employee_count_assignees.py”. 
-	Next I calculated the highly matched df within the assignee using the file “showing_match_ability_on_subset_large_company.py” run on the super computer. 
-	For the top 5k files I used “showing_match_ability_on_subset_large_company_slice.py” which takes a company from the null data set and subsets it by assignee ID to loop through and calculate the fuzzy ratio just inside a company. If the merge was larger than 25m rows it was cancelled and that company was added to a list of very large companies
-	The top 347 largest companies were then run in chunks using the file “calculating_imputed_country_from_assignee_top5k_after_calculating_on_sample.py”
-	The last few largest companies were broken down and distributed among the Compute Canada clusters. Total run time with lots of manual job submission was 45 days. Quadrillions of calculations were made. 


Append all the Results from the Assignee Imputation:
-	Ideally this would just have one output, but the size of the data led me to create 3 separate iterations with thousands of files per method. 
-	To append all of them together using my method, I used the file “appending_assignee_imputed_country_files.py”
-	This reads in around 100 csvs at a time before dumping out a chunk of 100 appended csvs. These are all brought into one csv at the end.
-	The resulting file is “master_highly_matched_data_set.csv” which is a ~211 million row csv. It will contain many matches per inventor, so the next step cleans up the matches to return just one country code per inventor

Clean up the highly_matched (from assignee) data set:
-	Uses the “from_highly_matched_reduces_all_matches_to_closest_date.py” file
-	Takes in the “master_highly_matched_data_set.csv” from the step above and narrows down to one country per inventor. 
-	First, this file cleans up erroneous dates and writes out a new highly matched file called “master_highly_matched_data_set_clean.csv” 
-	The dates are parsed into datetime objects and the distance between days are measured from the application with an unknown country code and all the matched inventors it finds. 
-	The minimum distance date is then used as the new country code. Where duplicates exist a random draw is taken from the set of possible country codes. 
-	The country code is then assigned to the appln_psn_id as to randomize any error between a name on an application and not just a name in a company.


Merge assignee-imputed country codes onto the main data set:
-	Returning to the file “imputed_country_code_merging_to_master.py”, import the “data_master_with_merged_imputed_efid.csv” file and the “imputed_assignee_results_by_appln_psn_id_and_country_code.csv”
-	Merge the imputed assignee csv onto the master by the appln_psn_id number
-	Export as the new master data set. 
-	**Note**: the column name ‘imputed_country_efid_y’ is a bit misleading as it is really the imputed country from the within-assignee method of computing. This is changed in the Canadian Ownership Analysis step. 

 Second Loop EFID Preprocessing:
-	Using the “preprocessing_for_same_invention_country_assigning.py” file, run the “#Exports From Imputed Assignee by EFID#” section to send out the null and not null datasets named “any_null_country_inventor_only_from_assignee_results_use_for_second_EFID_loop.csv” and “NOT_null_country_inventor_only_from_assignee_results_use_for_second_EFID_loop.csv”

Second Loop EFID check:
-	Use the “matching_within_efid_second_loop.py” file
-	It reads in the null and not null sets and attempts to fuzzy match within EFIDs a second time, using the newly imputed country codes. 
-	Results will have multiple possible matches which need to be cleaned and reduced to the closest date.
-	Results sent to “Second EFID Loop/highly_matched_df_from_second_loop_efid.csv”

Second Loop EFID cleanup:
-	Use the “efid_second_loop_closest_date_cleanup.py” file
-	Creates the days between columns
-	Drops duplicates within the application, considers only the minimum days between observations per appln_psn_id, randomly draws from available countries when minimum days between draw. 
-	Results sent to “Final Data Sets/imputed_assignee_results_by_appln_psn_id_and_country_code_from_second_loop.csv”
 
Canadian Ownership Analysis:
-	Use the “canadian_ownership_analysis_on_all_3_code_types.py” file
-	Reads in a dataframe and determines which inventions are owned by Canadian inventors by checking to see what the majority of countries are on an innovation and returning those with >50% Canadians. 
-	Writes out 5 .txt files with a list of earliest filing ID’s to be queried off the server. 

Retrieve Basic Patent Data:
-	Use the “query_database_for_Canadian_inventions.py” file
-	This will return a basic data set, and a technology classification data set from saved as “Canadian_inventions_determined_by_second_loop.csv” and “Canadian_inventions_determined_by_second_loop_NACE.csv”
-	Runs queries to IST’s server, depending on how the PATSTAT database is stored this step will be slightly different for recreation. 

R&D Measures Creation:
-	Much of this work was done by manually parsing a small PDF into a few csvs and merging it with another csv to calculate out a research intensity variable. 
-	Uses the file “data_cleanup_from_CA_value_added.py”
-	Uses the “raw_CA_Value_added_by_isic.csv”
-	Uses “raw_CA_Output_by_isic.csv”
-	Uses “raw_CA_R&D_expenditure_ANBERD_OECD.csv”
-	This dumps out the “research_intensity_metric_VA.csv” and the “research_intensity_metric_from_output.csv” 
-	I adjusted the “10, 11, &12” rows to be one of each 10, 11, and 12 manually. 

Regression Set Creation (Canadians):
-	Uses the “creating_regression_data_set.py” file
-	Uses the “Canadian_inventions_determined_by_second_loop.csv”
-	Uses the “Canadian_inventions_determined_by_second_loop_NACE2.csv”
-	Uses the “NACE2_Table.csv”
-	Uses the “Research Intensity Measures/research_intensity_metric_from_output.csv”
-	Does some cleanup by converting certain objects to date times, drops some columns. 
-	Creates the regression variables:
o	Application intensity measure
o	Innovation intensity measure
o	Drops NaN intensity applications where the tech field intensities are not shown. 
o	Creates a dummy column for every technology field 
o	Create a dummy column for every NACE2 field
o	Cleanup and apply an ownership variable to each application
o	Create Canada exclusive variable
o	Drop rows which were added in to account for person names, resulting in multiple rows per application. The drop process will create one row per application with all of the technology field, intensity, ownership and exclusivity variables
-	Produces the “master_data_set_canadian_innovations_for_regression_analysis.csv” output
-	Outline recoding for PSN_SECTOR variable

Regression Set Creation (Full Set):
-	Uses the “query_database_for_full_data_specs.py” file
-	Performs most of the same operations as the Canadian one, but breaks the tables down into separate files to save memory. 
-	Write out files are:
-	“intensity_data.csv” research intensity per application ID, intensity on the application and innovation level
-	“main_data.csv” basic data from tls201
-	“nace_data.csv” the NACE level data per application. Not in percentage basis (smaller df)
-	“nace_data_percentage_dummies.csv” NACE data per application with ‘dummy’ percentage variables. 
-	“nace_with_intensity_data.csv” combination of the NACE data on the application level and the intensity data on the application and innovation level. Not broken down into percentages. 
-	“person_data.csv” person data per application, resulting in many-many relationships
-	“publn_data.csv” application publication data, used for finding grant date and subsequently days between application and grant
-	“query_continuation.csv” continuation data on application number
-	“techn_data.csv” technology class per application. Not in percentage basis (smaller df)
-	“techn_data_percentage_dummies.csv” technology class data per application with ‘dummy’ percentage variables
