"""
Bring in the raw value added and output data
Bring in the R&D data
Clean them up and merged
Export the files
In the exported files I manually added in 10, 11, and 12 rows
"""


import pandas as pd
import numpy as np

value_added = pd.read_csv("C:/Users/ccearle/Documents/cipo-paper.git/Research Intensity Measures/raw_CA_Value_added_by_isic.csv", dtype={"isic":"int64"})

def dataCleanup(data):
    data["value"] = data["value"].replace("...", np.nan)
    data["value"] = data["value"].astype("float64")
    data.dtypes
    data[data["isic"] > 999]
    data["isic_short"] = data['isic'].apply(lambda x: str(x)[0:2])
    data2 = pd.DataFrame(data[["year", "isic", "isic_short", "value"]].groupby(["year", "isic_short"])["value"].sum())["value"].astype("int64").reset_index()
    data2 = data2[(data2["year"] >2008) & (data2["year"] < 2016)]
    return data2

value_added_clean = dataCleanup(value_added)

def dataAveraging(input_data):
    #get the average value
    data_average = pd.DataFrame(input_data.groupby("isic_short")['value'].mean()).reset_index()
    #convert to int for merging
    data_average["value"] = data_average["value"].astype("int64")
    data_average = data_average.append(pd.DataFrame({"isic_short":["10 & 11 & 12"], "value":[data_average[(data_average["isic_short"] == "10") | (data_average["isic_short"] == "11") | (data_average["isic_short"] == "12")].value.sum()]}))
    data_average = data_average[(data_average["isic_short"] != "10") & (data_average["isic_short"] != "11") & (data_average["isic_short"] != "12")]
    data_average.rename(columns={"isic_short":"industry", "value":"value_added"}, inplace = True)
    data_average = data_average[data_average["value_added"] > 0]
    return data_average


value_added_average =  dataAveraging(value_added_clean)

output = pd.read_csv("C:/Users/ccearle/Documents/cipo-paper.git/Research Intensity Measures/raw_CA_Output_by_isic.csv", dtype={"isic":"int64"})

output_clean = dataCleanup(output)

output_average = dataAveraging(output_clean)


#bring in the R&D Data
data_rd = pd.read_csv("C:/Users/ccearle/Documents/cipo-paper.git/Research Intensity Measures/raw_CA_R&D_expenditure_ANBERD_OECD.csv", dtype={"industry":"object"})

def R_and_D_cleanup(data):
    data = data[["industry", "industry_name", "rd_average"]]
    data["rd_average_full_amnt"] = data["rd_average"]*1000000
    return data

data_rd_clean = R_and_D_cleanup(data_rd)

def merger(data_average, data_rd_clean):
    data_average_rd = pd.merge(left = data_average, right = data_rd_clean, on = "industry", how = 'inner')
    data_average_rd["intensity"] = (data_average_rd["rd_average_full_amnt"]/data_average_rd["value_added"])*100
    data_average_rd.sort_values("intensity", inplace=True)
    return data_average_rd

merged_value_added = merger(value_added_average, data_rd_clean)
merged_output = merger(output_average, data_rd_clean)


merged_value_added.to_csv("C:/Users/ccearle/Documents/cipo-paper.git/Code/Clean Code/Research Intensity Measures/research_intensity_metric_VA.csv", index = False)

merged_output.to_csv("C:/Users/ccearle/Documents/cipo-paper.git/Code/Clean Code/Research Intensity Measures/research_intensity_metric_from_output.csv", index = False)
