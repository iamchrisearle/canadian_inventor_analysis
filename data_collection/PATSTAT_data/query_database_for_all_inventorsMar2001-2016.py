#####################################
##Querying the DB for all Inventors##
#####################################

"""
Code to query the DB to pull all inventors.
I did this in pieces as the set is huge.
This code would return the full data set.
"""

import pymysql
import paramiko
import pandas as pd
from paramiko import SSHClient
from sshtunnel import SSHTunnelForwarder
import time
####Increase display options for Pandas####
pd.options.display.max_columns = None
pd.options.display.max_rows = 150

#query for the selected fields for application filed between March 1st 2001 and Jan 1 2017
query = """SELECT tls201_appln.appln_id, appln_auth, appln_kind, appln_filing_date, appln_nr_original, earliest_filing_id,
earliest_filing_date, docdb_family_id, granted, nb_citing_docdb_fam, docdb_family_size,
nb_applicants, nb_inventors, tls206_person.person_id, person_name, person_address, person_ctry_code, doc_std_name,
psn_id, psn_name, psn_level, psn_sector, applt_seq_nr, invt_seq_nr
FROM tls201_appln
JOIN tls207_pers_appln ON tls201_appln.appln_id = tls207_pers_appln.appln_id
JOIN tls206_person ON tls207_pers_appln.person_id = tls206_person.person_id
WHERE earliest_filing_date >= "2001-03-01" AND earliest_filing_date < "2017-01-01";
"""

#This is specific to the server that hosts PATSTAT.
def spinUp():
    ####Path in####
    path = ""
    ####Port in Credentials####
    with open(path + "ssh_host.pem", 'r') as file:
        ssh_host = file.readline()
    with open(path + "ssh_user.pem", 'r') as file:
        ssh_username = file.readline()
    with open(path + "ssh_password.pem", 'r') as file:
        ssh_password = file.readline()
    with open(path + "mysql_username.pem", "r") as file:
        mysql_username = file.readline()
    with open(path + "mysql_password.pem", "r") as file:
        mysql_password = file.readline()
    with open(path + "mysql_db.pem", "r") as file:
        mysql_db = file.readline()
    ####Tunnel In####
    tunnel = SSHTunnelForwarder(('econ-patstat.uwaterloo.ca', 22), ssh_username= ssh_username, ssh_password = ssh_password, remote_bind_address=("localhost", 3306))
    tunnel.start()
    time.sleep(2)
    conn = pymysql.connect(host='127.0.0.1', user = mysql_username, passwd = mysql_password, db = mysql_db, port=tunnel.local_bind_port)
    return conn

#This query will pull all the inventors on all applications in one go
#For slower machines, consider breaking this up by year and dumping the results out a csv to append together later
results = pd.read_sql_query(query.replace("\n", " "), spinUp())

results.to_csv("~/canadian_inventor_analysis.git/data_cleanup_and_feature_engineering/allInventors2001-2016WithPSN.csv")

#export the results afterwards
