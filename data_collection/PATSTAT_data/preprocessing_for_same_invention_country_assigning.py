"""
Takes in the assignee ID dataset create in the assignee_matching_on_full_data.py file
Runs some calculation to provide summary statistics
"""

import pandas as pd
import numpy as np
from fuzzywuzzy import fuzz
import itertools
import time
import seaborn as sns
import matplotlib as plt
%matplotlib inline
pd.options.display.max_columns = None
pd.options.display.max_rows = 500
pd.set_option("mode.chained_assignment", None)

#########################################
##Cleaning Data From Just Assignee File##
#########################################

#assignee text


#assignee as ids
data = pd.read_csv("allInventors2001-2016WithPSN_with_Assignee_IDs.csv", low_memory=False)

#data cleanup
data.drop(columns = "Unnamed: 0", inplace=True)
data.drop_duplicates(inplace = True)
#data = data[data["invt_seq_nr"] > 0]
data.head()

#####################
####Indexing Data####
#####################
data.dtypes

def indexCreator(df):
    """Requires DF with appln_id and psn_id columns"""
    df["appln_psn_id"] = df[["appln_id", "psn_id"]].apply(lambda x: int(f"{x['appln_id']}{x['psn_id']}"), axis = 1)
    return df

data = indexCreator(data)

#Export with Assignee Name
data.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/allInventors2001-2016WithPSN_with_Indexer_and_Assignee.csv")

#export with Assignee ID
data.to_csv("D:/PATSTAT Result Data Sets/Final Data Sets/allInventors2001-2016WithPSN_with_Indexer_and_Assignee_ID.csv")

##############################
##Indexing Data And Graphing##
##############################

data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/allInventors2001-2016WithPSN_with_Indexer_and_Assignee_ID.csv")


def summaryStatistics(df):
    na_inventions = set(df[df["person_ctry_code"].isnull()]["earliest_filing_id"])
    not_na_inventions = set(df[df["person_ctry_code"].notnull()]["earliest_filing_id"])
    na_applications = set(df[df["person_ctry_code"].isnull()]["appln_id"])
    not_na_applications = set(df[df["person_ctry_code"].notnull()]["appln_id"])
    total_inventions = len(set(df["earliest_filing_id"]))
    total_applications = len(set(df["appln_id"]))
    print(f"There are {total_inventions} inventions")
    print(f"There are {total_applications} applications")
    print(f"There are {len(na_inventions - not_na_inventions)} inventions which contain all null country codes")
    print(f"There are {len(na_inventions.intersection(not_na_inventions))} inventions which contain some null and some non-null country codes")
    print(f"There are {len(not_na_inventions - na_inventions)} inventions which contain all non-null country codes")
    print(f"There are {len(na_applications - not_na_applications)} applications which contain all null country codes")
    print(f"There are {len(na_applications.intersection(not_na_applications))} applications which contain some null and some non-null country codes")
    print(f"There are {len(not_na_applications - na_applications)} applications which contain all non-null country codes")

summaryStatistics(data)

#graphing missing data
def graphMissingData(df):
    plotting_cc = df[df["appln_id"].isin( (set(df[df["person_ctry_code"].isnull()]["appln_id"])) - set(df[df["person_ctry_code"].notnull()]["appln_id"]) )]
    plotting_cc.drop_duplicates("appln_id", inplace = True)
    return sns.countplot(x = plotting_cc["appln_auth"], order=plotting_cc.appln_auth.value_counts().iloc[:7].index)

graphMissingData(data)


###########################################
##Exports Based On Certain Data Quadrants##
###########################################


##Export Data to be Chuncked In instead##
def mixedDataExport():
    """Subsets the dataframe into inventions with mixed null/not-null country codes
    Splits the mixed data into null and non-null country data and exports it for later analysis
    Corresponds to Data Quadrant 3"""
    mixed_inventions = set(data[data["person_ctry_code"].isnull()]["earliest_filing_id"]).intersection(set(data[data["person_ctry_code"].notnull()]["earliest_filing_id"]))
    mixed_inventions_df = data[data["earliest_filing_id"].isin(mixed_inventions)].copy()
    null_country = mixed_inventions_df[mixed_inventions_df["person_ctry_code"].isnull()]
    not_null_country = mixed_inventions_df[mixed_inventions_df["person_ctry_code"].notnull()]
    null_country.to_csv("D:/PATSTAT Result Data Sets/nullCountryCodesFromMixedEFIDNullNotNullSet.csv")
    not_null_country.to_csv("D:/PATSTAT Result Data Sets/NOTnullCountryCodesFromMixedEFIDNullNotNullSet.csv")

mixedDataExport()

def allNullExport():
    """Subsets and exports the dataframe into inventions with all null country code inventions
    Corresponds to Data Quadrant 1"""
    all_null = set(data[data["person_ctry_code"].isnull()]["earliest_filing_id"]) - set(data[data["person_ctry_code"].notnull()]["earliest_filing_id"])
    all_null_df = data[data["earliest_filing_id"].isin(all_null)].copy()
    all_null_df.to_csv("D:/PATSTAT Result Data Sets/allNullFromEFIDAllNullSet.csv")

allNullExport()

def notNullDataExport():
    """Subsets and exports all the data where there is some data.
    Any county level data can be used to ascertain a country code within an assignee
    Corresponds to Data Quadrant 4,6"""
    not_null_df = data[data["person_ctry_code"].notnull()]
    not_null_df.to_csv("D:/PATSTAT Result Data Sets/allNOTNullFromAllData.csv")

notNullDataExport()


########################################
##Exports From Imputed Country by EFID##
########################################

data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/data_master_with_merged_imputed_efid.csv", low_memory=False)
data.head()

def assigneeAnalysisExports(data):
    """Subsets and exports the dataframe into applications with any null country code
    Corresponds to Data Quadrant 1 and 2. Has some data from 3 and 4"""
    null_country = data[(data["imputed_country_efid"].isnull()) & (data["invt_seq_nr"] > 0)]
    null_country.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/any_null_country_inventor_only.csv")
    """Then subsets and exports any data where a country code was given
    Corresponds to Data Quadrants 2 and some of 4"""
    not_null_country = data[data["imputed_country_efid"].notnull()]
    not_null_country.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/not_null_country.csv")

assigneeAnalysisExports(data_with_imputed)


data = pd.read_csv("D:/PATSTAT Result Data Sets/Final Data Sets/allInventors2001-2016WithPSN_with_Indexer_and_Assignee.csv", low_memory=False)
##Assigneed analysis date, but using the person country code from PATSTAT, not our calculated one

def assigneeAnalysisExports(data):
    """Subsets and exports the dataframe into applications with any null country code
    Corresponds to Data Quadrant 1 and 2. Has some data from 3 and 4"""
    null_country = data[data["person_ctry_code"].isnull()]
    null_country.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/any_null_country_assignee_ID_process_from_PATSTAT_cc.csv")
    """Then subsets and exports any data where a country code was given
    Corresponds to Data Quadrants 2 and some of 4"""
    not_null_country = data[data["person_ctry_code"].notnull()]
    not_null_country.to_csv("D:/PATSTAT Result Data Sets/Assignee Imputing/not_null_country_assignee_ID_process_from_PATSTAT_cc.csv")

assigneeAnalysisExports(data)
